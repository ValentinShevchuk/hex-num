var HexGame = HexGame || {};

HexGame.Unit = function(state, data) {
	// находим реальные координаты
	var position = state.board.getXYFromRowCol(data.row, data.col);

	//создаем юнита
	Phaser.Sprite.call(this, state.game, position.x, position.y, data.asset);

	this.game = state.game;
	this.state = state;
	this.board = state.board;
	this.row = data.row;
	this.col = data.col;
	this.data = data;

	// центруем
	this.anchor.setTo(0.5);

	//this.inputEnabled = true;
	//this.input.pixelPerfectClick = true;
	//this.events.onInputDown.add(this.showMovementOptions, this);
};

HexGame.Unit.prototype = Object.create(Phaser.Sprite.prototype);
HexGame.Unit.prototype.constructor = HexGame.Unit;

HexGame.Unit.prototype.showMovementOptions = function(){
// очищаем все клетки
this.state.clearSelection();

	//only if the UI is free
	if(this.state.uiBlocked) {
		return;
	}

	//get current tile
	// получаем инфо об ячейке
	var currTile = this.board.getFromRowCol(this.row, this.col);

	//get the adjacent cells
	// получаем возможные ходы
	var adjacentCells = this.board.getAdjacent(currTile, true);

	// подсвечиваем ход
	adjacentCells.forEach(function(tile){
		tile.alpha = 0.7;

		//add input
		tile.events.onInputDown.add(this.moveUnit, this);
	}, this);
};

HexGame.Unit.prototype.moveUnit = function(tile){
	this.state.clearSelection();

	this.state.uiBlocked = true;

	// получаем координаты новой цели
	var pos = this.board.getXYFromRowCol(tile.row, tile.col);
	// создаем анимацию
	var unitMovement = this.game.add.tween(this);

	unitMovement.to(pos, 200);
	unitMovement.onComplete.add(function(){
		this.state.uiBlocked = false;
		this.row = tile.row;
		this.col = tile.col;

		//check for battles
 		this.checkBattle();
		//check for game ending
    this.state.checkGameEnd();
		//prepare unit
		this.state.prepareNextUnit();
	}, this);
	unitMovement.start();
};

HexGame.Unit.prototype.attack = function(attacked) {
	// в функцию передается цель для атаки

	//атакуемый
	var attacker = this;

	//both units attack each other
	var damageAttacked = Math.max(0, attacker.data.attack * Math.random() - attacked.data.defense * Math.random());
	var damageAttacker = Math.max(0, attacked.data.attack * Math.random() - attacker.data.defense * Math.random());

	attacked.data.health -= damageAttacked;
	attacker.data.health -= damageAttacker;

	if(attacked.data.health <= 0) {
		attacked.kill();
	}

	if(attacker.data.health <= 0) {
		attacker.kill();
	}
};

HexGame.Unit.prototype.checkBattle = function() {
  //get rival army
	// определяем кто на кого нападает, если игрок то противник ГруппаВрагов, иначе отбивается группаИгрока
  var rivalUnits = this.isPlayer ? this.state.enemyUnits : this.state.playerUnits;

  var fightUnit;

  //check rival army units to find a match
	// провереям группу есть ли кто из юнитов на точке с нападавшим
  rivalUnits.forEachAlive(function(unit){
    if(this.row === unit.row && this.col === unit.col) {
      console.log('both are in the same cell! -- fight!!!');
      fightUnit = unit;
    }
  }, this);

  //fight until death
  if(fightUnit) {
    while(this.data.health >= 0 && fightUnit.data.health >= 0) {
      this.attack(fightUnit);
    }
    console.log('battle end');
  }
};

// передача хода
HexGame.Unit.prototype.playTurn = function() {
  if(this.isPlayer) {
		// если игрок, то играет человек
    this.showMovementOptions();
  }
  else {
		// иначе делает ход компьютер
    this.aiEnemyMovement();
  }
};

// ход противника
HexGame.Unit.prototype.aiEnemyMovement = function() {
  //clear previous selection
	// очищаем все ячейки
  this.state.clearSelection();

  //get the current tile
	// получаем ячейку
  var currTile = this.board.getFromRowCol(this.row, this.col);

  //get the adjacent adjacent
	// получаем ходы
  var adjacentCells = this.board.getAdjacent(currTile, true);

  //target tile
  var targetTile;

  //go through each adjacent cell and find a rival
	// находим нет ли соперника в ближайших точках
  adjacentCells.forEach(function(tile){
    //find out if there is a rival in there
		//проверяем ячейку со всеми живыми игроками
    this.state.playerUnits.forEachAlive(function(unit){
      if(tile.row === unit.row && tile.col === unit.col) {
				
        console.log('we have found a rival to attack');
				// если есть то передаем объект для дальнейшей битвы
        targetTile = tile;
      }
    }, this);
  }, this);

  //if you didnt find a rival, then move randomly
	// есди цель не найдена движемся в случайном порядке
  if(!targetTile) {
    var randomIndex = Math.floor(Math.random() * adjacentCells.length);
    targetTile = adjacentCells[randomIndex];
  }

  //move to the target
  this.moveUnit(targetTile);
};
var HexGame = HexGame || {};
HexGame.Block = function(state, x, y,param) {
  var param=param?param:{};
  this.state = state;
	Phaser.Sprite.call(this, state.game, x, y, param.asset);
};

HexGame.Block.prototype = Object.create(Phaser.Sprite.prototype);
HexGame.Block.prototype.constructor = HexGame.Block;
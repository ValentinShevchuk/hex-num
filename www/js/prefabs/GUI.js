var mini = mini || {};
mini.GUI = function(game) {
  this.state = game;


  this.overlay = this.state.add.bitmapData(this.state.game.width, this.state.game.height);
  this.overlay.ctx.fillStyle = '#000';
  this.overlay.ctx.fillRect(0, 0, this.state.game.width, this.state.game.height);  

  this.panel= new Phaser.Sprite(this.state.game, 0, 0, this.overlay);
  this.panel.alpha = 0.8;
  this.panel.fixedToCamera = true;    
   
  //this.showMainMenu();
   // this.Elevator();
  //  this.Die();
  //  this.Help();
  //  this.vanishMessage();
  

  
  //  this.showPanel(this.groupHelp);
   // this.vanishMessage('Убил', this.state.player);
};

mini.GUI.prototype.showMainMenu = function() {
  this.GUIGroup = this.state.add.group();
  this.menuIcon = this.state.add.sprite(this.state.game.width - 40, 10, 'images');
  this.menuIcon.frameName='icon/list.png';
  //  this.menuIcon.scale.setTo(0.2);

  //show quests when you touch the quests icon
  this.menuIcon.inputEnabled = true;
  this.menuIcon.events.onInputDown.add(  function(){
    this.state.state.start('Menu');
  }, this);
  this.GUIGroup.add(this.menuIcon);
  this.GUIGroup.fixedToCamera = true;
  this.state.game.world.bringToTop(this.GUIGroup);

  
};


mini.GUI.prototype.Elevator = function() {
   
 
  
  var styleTitle = {font: '24px Arial', fill: '#fff'},
  styleText= {font: '14px Arial', fill: '#fff'};
// window lift

// белая кнопка
  this.btn = this.state.add.bitmapData(50, 50);
  this.btn.ctx.fillStyle = '#fff';
  this.btn.ctx.fillRect(0, 0, 50, 50);
      
  
  this.groupElevator = this.state.add.group();
  this.groupElevator.y =this.state.game.height;
  
  var panel= new Phaser.Sprite(this.state.game, 0, 0, this.overlay);
  panel.alpha = 0.8;
  panel.fixedToCamera = true;    

  this.groupElevator.add(panel);

  this.closeIconElevator = this.state.add.sprite(this.state.game.width - 40, 10, 'closeIcon');
  this.closeIconElevator.fixedToCamera = true;       
  this.closeIconElevator.inputEnabled = true;
  this.groupElevator.add(this.closeIconElevator); 
  
   this.closeIconElevator.events.onInputDown.add(
    function(){
      this.hidePanel(this.groupElevator);
    }, this);
  
  
   
  this.label = this.state.game.add.text(10, 10, 'Лифт', styleTitle);
  this.label.fixedToCamera = true;
  this.groupElevator.add(this.label);  

   
  this.label = this.state.game.add.text(10, 50, 'Текущий уровень: '+this.state.level,  {font: '14px Arial', fill: '#fff'});
  this.label.fixedToCamera = true;
  this.groupElevator.add(this.label);  
  
  
  this.label = this.state.game.add.text(10, 110, 'Выберите уровень', styleText);
  this.label.fixedToCamera = true;    
  this.groupElevator.add(this.label);    


 /**/
function createButton(this_,param){
  
  var button= this_.state.game.add.button(param.x, param.y,this_.btn, param.call, this_);    
  button.fixedToCamera = true;    
  this_.groupElevator.add(button);  
  var label = this_.state.game.add.text(button.x+button.width/2, button.y+button.height/2, param.labelText, {font: '26px Arial', fill: '#000'}); 
  label.anchor.setTo(0.5);
  label.fixedToCamera = true;    
   
  this_.groupElevator.add(label);   
} 
//var callElevator=function(){ this.state.game.state.start('Game',true,false, 1); }
 createButton(this,{x:10, y:160, labelText:0, call: function(){ this.state.game.state.start('level0',true,false); }});
createButton(this,{x:70, y:160, labelText:1, call: function(){ console.log(1); this.state.game.state.start('Game',true,false, 1); }});
createButton(this,{x:130, y:160, labelText:2, call: function(){ this.state.game.state.start('Game',true,false, 2); }});
// createButton(this,{x:60, y:10, labelText:10, call: callElevator});   
  
   
   
};

mini.GUI.prototype.Die = function() {

  var Group=this.state.add.group();	
  Group.inputEnabled = true;
  Group.y=this.state.game.height;
  
  var panel= new Phaser.Sprite(this.state.game, 0, 0, this.overlay);
  panel.alpha = 0.8;
  panel.fixedToCamera = true;    

  Group.add(panel);
  
  var label = this.state.game.add.text(this.state.game.width/2, 50, 'Вы убиты',  {font: '20px Arial', fill: '#ff0700'});
  label.fixedToCamera = true;
  label.anchor.setTo(0.5);
  Group.add(label);


  this.btn = this.state.add.bitmapData(100, 50);
  this.btn.ctx.fillStyle = '#fff';
  this.btn.ctx.fillRect(0, 0, 100, 50);
  
 
//-----------

  var call=function(){    localStorage.clear(); this.state.game.state.start('Game'); }
  var button= this.state.game.add.button(this.state.game.width/2, 150,this.btn, call, this); 
  button.anchor.setTo(0.5);   
  button.fixedToCamera = true;    
  Group.add(button);  
  
  var label = this.state.game.add.text(button.x, button.y, 'Начать сначала', {font: '26px Arial', fill: '#000'});    
  label.fixedToCamera = true;  
  label.anchor.setTo(0.5);   
  button.width=label.width+20;    
  button.height=label.height+20;    
  Group.add(label);  
  
   // label.anchor.setTo(0.5);
//------------   

//-----------
/*
  var call=function(){ 
    localStorage.setItem('currentLevel', -1);
    this.state.game.state.start('Game',true,false,'level0_0'); 
  }

  var button= this.state.game.add.button(150, 100,this.btn,call,this);    
   
   
  button.fixedToCamera = true;    
  Group.add(button);  
  
  label = this.state.game.add.text(button.x+button.width/2, button.y+button.height/2, 'На верх', {font: '26px Arial', fill: '#000'}); 
  label.anchor.setTo(0.5);
  label.fixedToCamera = true;  
  Group.add(label);   */
//------------   
  



  
    
  this.groupDie = this.state.add.group();
  this.groupDie.y =this.state.game.height;
  this.groupDie=Group;
};

mini.GUI.prototype.Help = function() {

  var Group=this.state.add.group();	
  Group.inputEnabled = true;
  Group.y=this.state.game.height;
  
  var panel= new Phaser.Sprite(this.state.game, 0, 0, this.overlay);
  panel.alpha = 0.8;
  panel.fixedToCamera = true;    

  Group.add(panel);
  
  var label = this.state.game.add.text(this.state.game.width/2, 50, 'Цель игры',  {font: '26px Arial', fill: '#fff'});
   
  label.anchor.setTo(0.5); 
  label.fixedToCamera = true;
  Group.add(label);


  
  label = this.state.game.add.text(10, 75, '1. Найти потайную дверь\n2. Найти ключ\n3. Пройти через потайную дверь',  {font: '16px Arial', fill: '#00ff00'});
  
  //label.anchor.setTo(0.5);
  label.fixedToCamera = true;
  Group.add(label);

 
   

//-----------
/*
  var call=function(){ 
    localStorage.setItem('currentLevel', -1);
    this.state.game.state.start('Game',true,false,'level0_0'); 
  }

  var button= this.state.game.add.button(150, 100,this.btn,call,this);    
   
   
  button.fixedToCamera = true;    
  Group.add(button);  
  
  label = this.state.game.add.text(button.x+button.width/2, button.y+button.height/2, 'На верх', {font: '26px Arial', fill: '#000'}); 
  label.anchor.setTo(0.5);
  label.fixedToCamera = true;  
  Group.add(label);   */
//------------   
  


    panel.inputEnabled = true;
    panel.events.onInputDown.add(function(){
  
      this.hidePanel(Group);
    }, this);		
  
    
  this.groupHelp = this.state.add.group();
  this.groupHelp.y =this.state.game.height;
  this.groupHelp=Group;
};

mini.GUI.prototype.showPanel= function(panelGroup) {
  //console.log(this.state);
//this.state.game.paused = true;

panelGroup.y=0;
  //player can't move anymore
  this.state.uiBlocked = true;

this.state.game.physics.arcade.isPaused = (this.state.game.physics.arcade.isPaused) ? false : true;
   /* */  
  var showPanelTween = this.state.add.tween(panelGroup);
  showPanelTween.to({y: 0}, 150);

  //show the quests when the panel reaches the top
  showPanelTween.onComplete.add(function(){
    //show all quests
   // var questsText = 'QUESTS\n';

    //iterate through all the player quests
   
  }, this);

  showPanelTween.start(); 

  },
mini.GUI.prototype.hidePanel= function(panelGroup) {
/*   */  
this.state.game.physics.arcade.isPaused = (this.state.game.physics.arcade.isPaused) ? false : true;
   panelGroup.y = this.state.game.height;
  //release the UI
  this.state.uiBlocked = false;
  
  }
  
mini.GUI.prototype.test= function() {
console.log('Запущена функция test()');
  
  }
  
mini.GUI.prototype.vanishMessage= function(text, object,  param) {
  
  var object=object?object:this.state.player;

  var param = param || {};
  
  var style=param.style?param.style:{font: '16px Arial', fill: '#ff0000'};
  this.vanishLabel = this.state.game.add.text(object.x, object.y, text, style); 
if (this.vanishLabel.width+this.vanishLabel.x>this.state.game.width)
  this.vanishLabel.x=this.state.game.width-this.vanishLabel.width-10;
   //this.vanishLabel.anchor.setTo(0.5);
  
  var showPanelTween = this.state.add.tween( this.vanishLabel);
   //if (param.type=='alert') 
     showPanelTween.to({y: object.y-100, alpha:0},2000);
  // else showPanelTween.to({y: +1, alpha:0},4000,"Linear", true);

  showPanelTween.onComplete.add(function(){
     this.vanishLabel.alpha=0;
  }, this);

  showPanelTween.start(); 

}

mini.GUI.prototype.popupMessage= function(text, object,  param) {
    var group=this.state.add.group();	
  var object=object?object:this.state.player;

  var param = param || {};
  
  var panel= this.state.game.add.sprite( 0, 0, 'images');
  panel.scale.setTo(0.5*this.state.ZOOM);
  panel.frameName='other/bubble-rect.png';
  panel.x=this.state.game.width*0.5;//this.state.game.width-panel.width;
  panel.y=this.state.game.height*0.5;//this.state.game.height-panel.height;
  panel.anchor.setTo(0.5);
  group.add(panel);

  var style=param.style?param.style:{font: 20*this.state.ZOOM+'px Arial', fill: '#444', align:'center',wordWrap: true, wordWrapWidth: panel.width-15*this.state.ZOOM, };


  style.wordWrap=	style.wordWrap|| true;
  style.wordWrapWidth=style.wordWrapWidth || panel.width-40;

  var textLabel = this.state.game.add.text(panel.x, panel.y, text, style); 
  textLabel.anchor.setTo(0.5);
  //textLabel.fixedToCamera = true;  
    group.add(textLabel);
  var showPanelTween = this.state.add.tween( group);
   //if (param.type=='alert') 
     showPanelTween.to({ },3000);
  // else showPanelTween.to({y: +1, alpha:0},4000,"Linear", true);

  showPanelTween.onComplete.add(function(){
    group.alpha=0
  }, this);

  showPanelTween.start(); 

panel.inputEnabled = true;
  
  
     panel.events.onInputDown.add(
    function(){
      //	textLabel.destroy();
        group.destroy();
    }, this);
  group.fixedToCamera = true;  



}

mini.GUI.prototype.iconCreate= function(param){
    var param=param || {'x':0, 'y':0};
    var object={};

    param.scale=param.scale || this.state.ICON_ZOOM;
    param.anchor=param.anchor || 0.5;
    param.asset=param.asset || 'images';
    param.inputEnabled=param.inputEnabled || true;
    param.fixed =  param.fixed || false;

    object = this.state.game.add.sprite( param.x ,param.y, param.asset );
    if (param.frameName) object.frameName=param.frameName;
    object.scale.setTo(param.scale);
    object.anchor.setTo( param.anchor);
    object.inputEnabled =true;
    object.fixedToCamera = param.fixed;
    return object;
}
mini.GUI.prototype.textShadow = function(text, style){
    var style=style || {};
    style.shadow=style.shadow||false;
    text.stroke=style.stroke || "#de77ae";
    text.strokeThickness = style.strokeThickness || 8;
    if (style.shadow) text.setShadow(2, 2, "#333333", 2, true, false);

}
/**
 * @file Prefabs Board
 * @name Уаправление набором блоков
 * @author Valentin Shevchuk <birdvoron@yandex.ru>
 * @license MIT
 * @copyright 2017 unedge.ru
 */

var HexGame = HexGame || {};

/**
 * @class BoardCamp 
 * @param  {object} state - game окружение
 * @param  {array} grid - массив с числами и доп. параметрами
 * @return {object} возвращается группа объектов
 */

HexGame.BoardCamp = function(state, grid) {
    /**
     * HexGame.BoardCamp
     * Создает рабочее полее и реагирует на действия игрока другими методами
     * Принимается state и массив с полем
     */
    Phaser.Group.call(this, state.game);

    //цвета для затемнений
    this.colorB='#444'; 
    this.colorT='#fff';
    this.dark=0x777777;

    this.state = state;
    this.game = state.game;
    this.grid = grid;
    this.isDestroy=false;


    this.ZOOM=this.state.ZOOM;
    //	this.rows = 9;
    this.data={};
    this.color = [
        [
            "0x9999ff",
            "0xffcccc",
            "0xffccff",
            "0xccffff",
            "0xccffcc",
            "0xffffcc",
            "0x99ccff",
            "0x99cc99",
            "0xffff00"
        ],
        [
            "0x66cc99",
            "0xcc6666",
            "0xf9bb4b",
            "0x4da1ae",
            "0x32b7d1",
            "0x36717f",
            "0xe99785",
            "0xd8dabb",			
            "0x3cbda6",
        ],

        ];
    this.state.TILE_ROW=Math.ceil(this.grid.length/this.state.TILE_COL);
    var tempHeight=(this.state.TILE_ROW+3)*this.state.TILE_W;//tempHeight<this.state.game.height?this.state.game.height:tempHeight
    this.state.game.world.setBounds(0, 0, this.state.game.width,tempHeight);
    this.state.LAST_BOARD=this.grid;
    var i=-1;
    //create hexagons
    var row=0, col=-1, tile, x, y;
        var tempCol='';
    this.grid.forEach(function(data, index){
    
        if (index%this.state.TILE_COL==0 && index!=0 ){
            col=0; row++}
        else {col++;}

        if(row % 2 === 0) {
            x = this.state.MARGIN_X + col * this.state.TILE_W;
        }
        //odd rows
        else {
            x = this.state.MARGIN_X + col * this.state.TILE_W + this.state.TILE_W/2;
        }
        y = this.state.MARGIN_Y + row * this.state.TILE_H * 3/4;
        tile = new Phaser.Sprite(this.game, x, y,"images" );

        if (data.type==1) tile.frameName='block/t3.png'; 
        else tile.frameName='block/t0.png'; 
        tile.type=data.type;

        tile.tint ='0xffffff';
        tile.tintColor='0xffffff';
       
        tile.scale.setTo(0.67*this.ZOOM);

        tile.row = row;
        tile.col = col;
        tile.numId=index;

        this.state.LAST_BOARD[index].col=col;
        this.state.LAST_BOARD[index].row=row;
        this.state.LAST_BOARD[index].isDestroy=false;

        var numXY=this.getXYFromRowCol(row,col);
        var num = this.state.game.add.text(numXY.x, numXY.y, data.num,  {font: 22*this.ZOOM+'px Arial', fill: this.colorT});
        num.anchor.setTo(0.5);
        //this.state.textShadow(num,{ "stroke": "#777", "strokeThickness":5,});
        this.state.textShadow(num, {"strokeThickness":-1,  "shadow":true });
        tile.numLabel=num;

        tile.num=data.num;
        tile.click=0;

        // возможность кликать
        tile.inputEnabled = true;
        tile.input.pixelPerfectClick = true;
        /* */

        tile.events.onInputDown.add( this.blockClick, this);
            

        this.add(tile);
    },this);


this.anim=false;
};


HexGame.BoardCamp.prototype = Object.create(Phaser.Group.prototype);
HexGame.BoardCamp.prototype.constructor = HexGame.BoardCamp;

HexGame.BoardCamp.prototype.blockClick=function(tile){
    if (this.state.clickHexCounter==1){this.state.clickHexCounter=0}
    else if (!this.anim) {
        this.state.clickHexCounter++;

        //	if (this.emitter) this.emitter.destroy();

        this.state.userStat.clickAll++;
        if (tile.click==0 || tile.click==1){
            if (tile==this.state.Num1){
                /*
                if (this.state.iconDel.alpha<1){
                    this.state.undoSet();

                    this.state.LAST_BOARD[this.state.Num1.numId].alive=false;
                    
                    this.state.Num1.numLabel.destroy();
                    this.state.Num1.kill();

                    this.state.Num1.destroy();

                    this.state.userStat.coin=0;
                    if (HexGame.BootState.SOUND) this.state.soundBoom.play();
                    this.state.iconDel.alpha=1;
                }*/
                this.state.clearSelection();
                this.state.Num1={};
                this.state.Num2={};
            }
            else{
                this.state.clearSelection();
                
                tile.click=1;
                this.state.Num1=tile;
                this.state.Num2={};

                //this.setAll('tint',this.dark);
                //tile.tint=0xffffff;
                this.setAll('tint','0x000000');
                this.setAll('alpha','0.2');
                tile.alpha = 0.7;
                tile.tint=0xffffff;

                var adj = this.getAdjacent2(tile, true);
                adj.forEach(function(t){

                    t.alpha = 0.9;
                    t.tint=0xffffff;
                    //t.tint=0xffffff;

                    t.click=2;
                }, this);
            }
        }
        else if (tile.click==2){
            this.state.Num2=tile;
            if (this.state.Num1.num==this.state.Num2.num || this.state.Num1.num+this.state.Num2.num==10 || (this.state.Num1.num+this.state.Num2.num)%10==0)
            {
                //this.state.soundClick.sound.pause();
                if (HexGame.BootState.SOUND) this.state.soundClick.play();

                var xy=this.getXYFromRowCol(this.state.Num1.row,this.state.Num1.col);
                //this.state.GUI.vanishMessage('+1',xy,{  style:{ fill: '#fff', font:'14px Arial', align: 'center'}});

                this.state.undoSet();


                /*
                this.state.LAST_BOARD[this.state.Num1.numId].alive=false;
                this.state.LAST_BOARD[this.state.Num2.numId].alive=false;
                if (HexGame.BootState.ANIMATION) this.emi(this.state.Num1.x, this.state.Num1.y);
                this.state.Num1.numLabel.destroy();
                this.state.Num1.kill();
                
                this.state.Num2.numLabel.destroy();
                this.state.Num2.kill();
                
                this.state.Num1.destroy();
                this.state.Num2.destroy();
                */

                var anim1=this.state.game.add.tween(this.state.Num1);
                var anim2=this.state.game.add.tween(this.state.Num2);
                var anim11=this.state.game.add.tween(this.state.Num1);
                var anim22=this.state.game.add.tween(this.state.Num2);

                anim1.to({ alpha:0}, 500 , Phaser.Easing.Linear.None);
                anim2.to({ alpha:0}, 500 , Phaser.Easing.Linear.None);
                this.anim=true;
                anim1.onComplete.add(function(){

                    this.changeNum(this.state.Num1);
                    this.changeNum(this.state.Num2);
                   
                    this.state.Num1.numLabel.text=this.state.Num1.num;
                    this.state.Num2.numLabel.text=this.state.Num2.num;

                    anim11.to({ alpha:1}, 500, Phaser.Easing.Linear.None);
                    anim22.to({ alpha:1}, 500 , Phaser.Easing.Linear.None);
                    anim11.start();
                    anim22.start();
                    this.anim=false;


                    if(this.checkType())
                    {
                
                    } 
                    else 
                    {
                        this.forEach(function(data){
                            data.numLabel.kill();
                        },this);
                        this.removeAll();
                        this.state.win();
                    }

                }, this);
    

                anim1.start();
                anim2.start();
                            
                // сбрасывается контроль за добавлением
                this.state.CHEK_ADD_HEX=0;

                this.state.userStat.destroyCol+=2;
                //this.state.COIN+=1;
                if (this.state.hardcore) this.state.userStat.timeInGame+=this.state.PLUS_TIME;
                this.state.clearSelection();
                this.state.saveData();


                }else  this.state.clearSelection();
            }else {
            }
    }
}
HexGame.BoardCamp.prototype.changeNum = function(block) 
{
    // смена номера
    if (block.type==0) block.num=rand(1,9); 
    else 
    { 
        if (block.num>1) block.num--; 
        else 
        {
            block.type=0; 
            block.frameName='block/t0.png';
        }
    }
}

HexGame.BoardCamp.prototype.checkType = function() 
{
    // проверка наличия специальных блоков
    var check=0;
    this.forEach(function(data)
    {
        if (data.type) check++;
    },this);

    return check?true:false;
}
// метод возвращает спрайт по координатам клетки. принимаем y x
HexGame.BoardCamp.prototype.findPair = function(param) 
{
    var param=param || {};
    param.check= param.check || false;

    var stop=false, index=-1, find=false;
    while(!stop)
    {
        index++;
        var adj = this.getAdjacent2(this.children[index], true);
        adj.forEach(function(t){
            if ((this.children[index].num==t.num || this.children[index].num+t.num==10 ||  (this.children[index].num+t.num)%10==0) && !stop && (this.children[index].alive && t.alive)) {
                if (!param.check){	

                        this.setAll('tint','0x000000');
                        this.setAll('alpha','0.2');
                        //tile.tint=0xffffff;


                    this.children[index].alpha= 0.9;
                    this.children[index].tint=0xffffff;
                    t.alpha = 0.9;
                    t.tint=0xffffff;
                }
                stop=true;
                find=true;
            }
        }, this);

        
        if (index>this.total-2) stop=true;
        //console.log(index);
    }
    return find;
    //console.log('всего объектов',this.children.length);
};

// метод возвращает спрайт по координатам клетки. принимаем y x
HexGame.BoardCamp.prototype.getFromRowCol2 = function(row, col) {
  var foundTile;
  this.forEach(function(tile){
    if(tile.row === row && tile.col === col) {
      foundTile = tile;
    }
  }, this);

  return foundTile;
};

// метод возвращает спрайт по координатам клетки. принимаем y x
HexGame.BoardCamp.prototype.getFromRowCol = function(row, col) {
  var foundTile;

  this.forEach(function(tile){
    if(tile.row === row && tile.col === col) {
      foundTile = tile;
    }
  }, this);

  return foundTile;
};
// возвращет координаты
HexGame.BoardCamp.prototype.getXYFromRowCol = function(row, col){
  var pos = {};
  //even rows
  if(row % 2 === 0) {
    pos.x = this.state.MARGIN_X + col * this.state.TILE_W + this.state.TILE_W/2;
  }
  //odd rows
  else {
    pos.x = this.state.MARGIN_X + col * this.state.TILE_W + this.state.TILE_W/2 + this.state.TILE_W/2;
  }
  pos.y = this.state.MARGIN_Y + row * this.state.TILE_H * 3/4 + this.state.TILE_H/2;
  return pos;
};

// получение соседних клеток шесть направлений

HexGame.BoardCamp.prototype.getAdjacent2 = function(tile, rejectBlocked) {
  var adjacentTiles = [];
  var row = tile.row;
  var col = tile.col;

  var relativePositions = [];

  //relative positions of adjacent cells depend whether the row is odd or event
  //even rows

    if(row % 2 === 0) 
    {
        relativePositions = 
        [
            {r: -1, c: 0}, // левый верхний
            {r: -1, c: -1}, // 
            {r: 0, c: -1}, // левый
            {r: 0, c: 1},
            {r: 1, c: 0},
            {r: 1, c: -1}
        ]
    }

  //odd rows
    else 
    {
        relativePositions = 
        [
            {r: -1, c: 0},
            {r: -1, c: 1},
            {r: 0, c: -1},
            {r: 0, c: 1},
            {r: 1, c: 0},
            {r: 1, c: 1}
        ];
    }
    // нечетные
    //0 лево верх
    //1 право верх
    //2 лево
 
    //console.log(relativePositions);
  relativePositions.forEach(function(pos, index){
    var adjTile={};
    //проверяем, что мы не на краю карты
    if((row + pos.r >= 0) && (row + pos.r <=this.state.TILE_ROW) && (col + pos.c >= 0) && (col + pos.c <=this.state.TILE_COL) || index==2 || index==3) 
    { 
            //get adjacent tile
            var j=0;
            var tempRow=row+pos.r,  tempCol=col+pos.c;
            var jIndex=0;
            //if (index==2) //console.log(tempRow, tempCol);
            //		if (index==0 && row%2!==0) { tempRow=row-1; tempCol=col;}
            //	if (index==1 && row%2!==0) { tempRow=row-1; tempCol=col+1;}
            //if (index==1 && row%2==0) { tempRow=row-1; tempCol=col-1;}
            //if (index==0 && row%2!==0) { tempRow=row-1; tempCol=col; }

            while (j==0){
                jIndex++;
                j=0;
                if ((index==2 && tempCol<0) || index==3 && tempCol>this.state.TILE_COL-1) {
                    adjTile.alive=false;
                    //console.log('rhfq');
                }
                else adjTile = this.getFromRowCol2(tempRow , tempCol);

                if (adjTile==undefined) adjTile={"alive":false};

                if (adjTile.alive){ j=1
                } 

            //--------------------
            //верхнее правое чет
            else if (index==0 && row%2==0) {
                // работает
                    if (tempCol+1>this.state.TILE_COL&& tempRow%2!==0)j=1;
                    else if (tempRow%2!==0) tempCol+=1;  
                    if (tempRow-1<0) j=1;
                    else tempRow--; 
                    //console.log('учтено хлоп0 - чет', tempCol,tempRow);
            } 
                // верхнее левое не чет
                else if (index==0 && row%2!==0) {
                // работает
                if (tempCol-1<0 && tempRow%2==0)j=1;
                        else   if (tempRow%2==0) tempCol-=1; 
                        if (tempRow-1<0) j=1;
                        else tempRow--; 
                    //console.log('учтено хлоп0 - нечет', tempCol,tempRow);

                } 

                //----------------------
                // рабработае/
                //верхнее левое четное
                else if (index==1 && row%2==0) {
                        
                        if (tempCol-1<0 &&  tempRow%2==0)j=1;
                        else   if (tempRow%2==0) tempCol-=1; 
                        if (tempRow-1<0) j=1;
                        else tempRow--; 
            
                //console.log(' учтено хлоп1-чет', tempCol,tempRow );
                } 
                // эТАЛОН!!!!
                // верхнее правое не чет
                else if (index==1 && row%2!==0) {
                        //console.log('учтено хлоп1-нечет',tempCol);
                        if (tempCol+1>this.state.TILE_COL&& tempRow%2!==0)j=1;
                        else   if (tempRow%2!==0) tempCol+=1; 
                        if (tempRow-1<0) j=1;
                        else tempRow--; 
            

                } 
                //---------------------
                // левое нечет
                else if (index==2  && row%2!==0) {
                        //console.log('учтено хлоп2');
                    if (tempCol-1<0){ tempCol=this.state.TILE_COL-1;
                            if (tempRow-1<0) j=1;
                            else tempRow--; 
                    } else { tempCol--; }

                } 
                // левое чет
                else if (index==2  && row%2==0) {
                        //console.log('учтено хлоп2 чет');
                    if (tempCol-1<0){ tempCol=this.state.TILE_COL-1;
                            if (tempRow-1<0) j=1;
                            else tempRow--; 
                    } else { tempCol--; }

                } 
                //--------------------------
                // право не чет
                else if (index==3  && row%2==0) {
                    
                    if (tempCol+1>this.state.TILE_COL){
                            tempCol=0;
                            if (tempRow+1>this.rows-1) j=1;
                            else tempRow++; 
                    } else { tempCol++; }
                //	console.log('учтено хлоп3  чет', tempCol, tempRow);
                }
                // право чет
                else if (index==3  && row%2!==0) {
                    
                        if (tempCol+1>this.state.TILE_COL-1){ tempCol=0;
                            if (tempRow+1>this.rows-1) j=1;
                            else tempRow++; 
                    } else { tempCol++; }
                    //console.log('учтено хлоп3 не чет', tempCol, tempRow);
                }
                //--------------------
                // левый низ нечетно
                // работает
                else if (index==4  && row%2!==0) {
                    if (tempCol-1<0 &&  tempRow%2==0)j=1;
                    else   if (tempRow%2==0) tempCol-=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 
                            //console.log(' учтено хлоп4  не чет',tempCol, tempRow);
                }
                // правый низ четно 
                else if (index==4  && row%2==0) {
                    if (tempCol+1>this.state.TILE_COL&&  tempRow%2!==0)j=1;
                    else   if (tempRow%2!==0) tempCol+=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 
                    //console.log(' учтено хлоп4 чет',tempCol, tempRow );
                }
                //--------------------
                // правый низ нечетно
                //
                else if (index==5  && row%2!==0) {

                    //console.log(' учтено хлоп5  не чет');
                    if (tempCol+1>this.state.TILE_COL&&  tempRow%2==0)j=1;
                    else   if (tempRow%2!==0) tempCol+=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 		
                }
                // левыйƒ низ четно 
                // проверено
                else if (index==5 && row%2==0) {

                    if (tempCol-1<0 &&  tempRow%2==0)j=1;
                    else   if (tempRow%2==0) tempCol-=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 

                    //console.log('учтено хлоп5 чет');
                }
                //--------------------
                // правый низ четно
                //
                else if (index==6  && row%2==0) {

                //console.log(' учтено хлоп6  не чет');
                    if (tempCol+1>this.state.TILE_COL&&  tempRow%2!==0)j=1;
                    else   if (tempRow%2==0) tempCol+=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 		
                }
                // левыйƒ низ нечетно 
                //
                else if (index==6 && row%2!==0) {

                    if (tempCol-1<0 &&  tempRow%2==0)j=1;
                    else   if (tempRow%2!==0) tempCol-=1; 
                    if (tempRow+1>this.rows-1) j=1;
                    else tempRow++; 

                    //console.log(' учтено хлоп6 чет');
                }
                else { j=1; }


            //	if((tempRow + tempPosR >= 0) && (tempRow + tempPosR < this.rows) && (tempCol + tempPosC >= 0) && (tempCol + tempPosC < this.cols)){ j=1; //console.log('выход из цикла принуд '+	tempCol); }

                //if (index==2) //console.log('выход из цикла '+tempPosC);

                                if (jIndex>100) { j=1;  //console.log('экстренный выход'+(tempCol + tempRow));
                            }

            }

            //console.log(row%2);

            // проверяем что клетка не блокирована
     if( adjTile!==undefined && adjTile.alive) {
        adjacentTiles.push(adjTile);
      } else {
                //console.log('косяк');
            }
    }
  }, this);
  return adjacentTiles;
};

HexGame.BoardCamp.prototype.clearArray = function(board){
    var temp=[];
    board.forEach(function(data,index)
    {
        temp.push({"num":data.num, "alive":data.alive, "isDestroy": data.isDestroy});
    },this);
  return temp;
};


HexGame.BoardCamp.prototype.emi = function(x,y) {
    //Phaser.Sprite.prototype.kill.call(this);
    var emitter;
    if (this.state.time.fps>35)
    {
        emitter = this.state.game.add.emitter(x, y, 30);
        emitter.makeParticles('bubble');

        //this.emitter.minParticleSpeed.set(0, 100);
        // emitter.maxParticleSpeed.set(0, 600);

        emitter.setRotation(0, 0);
        emitter.setAlpha(0.6, 1, 3000);
        emitter.setScale(0.1, 0.4, 0.1, 0.4, 6000, Phaser.Easing.Quintic.Out);
        emitter.gravity = -200;
        emitter.start(true, 6000, null, 5);
        this.state.game.time.events.add(6000, function()
        {
            emitter.destroy();
        }, this);
    }
}

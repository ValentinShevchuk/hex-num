
var HexGame = HexGame || {};

//calculate the dimensions of the game so that 100% of the screen is occupied
HexGame.getGameLandscapeDimensions = function(max_w, max_h) {
  //get both w and h of the screen (some devices/browser measure this differntly, so you dont know for sure which one is which)
  //console.log(window.devicePixelRatio);
  var w = window.innerWidth * window.devicePixelRatio;
  var h = window.innerHeight * window.devicePixelRatio;
  
  //get the actual w and h. in landscape we'll define w as the longest one
  var landH = Math.max(w, h);
  var landW = Math.min(w, h);
  
  //do we need to scale to fit in width
  if(landW > max_w) {
    var ratioW = max_w / landW;
    landW *= ratioW;
    landH *= ratioW;
  }
  
  //do we need to scale to fit in height
  if(landH > max_h) {
    var ratioH = max_w / landW;
    landW *= ratioH;
    landH *= ratioH;
  }
  
  return {
    w: landW,
    h: landH
  }
}




function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function compareNumId(A, B) {
  return A.numId - B.numId;
}
var width__ = 350// window.innerWidth;
var height__ = 500//window.innerHeight;

//height__ = 1000;
//width__=height__/2;
 
this.ZOOM=1;
console.log(window.innerWidth, window.innerHeight, window.devicePixelRatio);
dim = HexGame.getGameLandscapeDimensions(480, 640);
//dim = HexGame.getGameLandscapeDimensions(384, 640);
//dim = HexGame.getGameLandscapeDimensions(480, 640);
var nameStorage = 'setting', setting={};
if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    setting = JSON.parse(localStorage.getItem(nameStorage));
    this.ZOOM=setting.ZOOM;
    console.log('загрузка настроек первая2...');
}
this.MAS=(1200/window.innerWidth);

this.ZOOM=1.5;
if (dim.w<(((56*this.ZOOM)*5))){
  //this.ZOOM-=this.ZOOM*
console.log('======',dim.w,((56*5)*this.ZOOM), window.innerWidth/((56*this.ZOOM)*5));
this.ZOOM-=this.ZOOM*(1-(window.innerWidth/(((56*this.ZOOM)*5.7))));

}


//this.ZOOM=1.5;
//width__*this.ZOOM, height__*this.ZOOM
HexGame.game = new Phaser.Game(dim.w,dim.h, Phaser.AUTO);
HexGame.game.ZOOM=this.ZOOM;
HexGame.game.DIM=dim;
HexGame.game.state.add('Boot', HexGame.BootState);
HexGame.game.state.add('Preload', HexGame.PreloadState);
HexGame.game.state.add('Home', HexGame.HomeState);
HexGame.game.state.add('Menu', HexGame.Menu);
HexGame.game.state.add('LockOriention', HexGame.	LockOriention);
HexGame.game.state.add('Help', HexGame.Help);
HexGame.game.state.add('Game', HexGame.GameState);
HexGame.game.state.add('Setting', HexGame.Setting);
HexGame.game.state.add('SelectGame', HexGame.SelectGame);
HexGame.game.state.add('Home2', HexGame.Home2);
HexGame.game.state.add('Shop', HexGame.Shop);
HexGame.game.state.add('Video', HexGame.Video);
HexGame.game.state.start('Boot');

// place our admob ad unit id here
var admobid = {};
if( /(android)/i.test(navigator.userAgent) ) {

  admobid = { // for Android
    banner: 'ca-app-pub-3663897682421444/4433763222',
    interstitial: 'ca-app-pub-3663897682421444~2957030020',
    rewardvideo:'ca-app-pub-3663897682421444/5893490022'
  };
} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
  admobid = { // for iOS
    banner: 'ca-app-pub-3663897682421444/4433763222',
    interstitial: 'ca-app-pub-3663897682421444~2957030020'
  };
} else {
  admobid = { // for Windows Phone
    banner: 'ca-app-pub-3663897682421444/4433763222',
    interstitial: 'ca-app-pub-3663897682421444~2957030020',
    rewardvideo:'ca-app-pub-3663897682421444/5893490022'
  };
}

function createSelectedBanner(){
  AdMob.createBanner({
    adId: admobid.banner,
    overlap: true,
    offsetTopBar: false,
    position: AdMob.AD_POSITION.BOTTOM_CENTER,
  });
}
function showBannerAtPosition(){
  AdMob.showBanner( AdMob.AD_POSITION.BOTTOM_CENTER );
}

function initAd(){
  console.log('запуск рекламы99s');
  AdMob.setOptions({
     adSize: 'SMART_BANNER',
    // width: integer, // valid when set adSize 'CUSTOM'
    // height: integer, // valid when set adSize 'CUSTOM'
    position: AdMob.AD_POSITION.BOTTOM_CENTER,
    // offsetTopBar: false, // avoid overlapped by status bar, for iOS7+
    bgColor: 'black', // color name, or '#RRGGBB'
    // x: integer,    // valid when set position to 0 / POS_XY
    // y: integer,    // valid when set position to 0 / POS_XY
    isTesting: false, // set to true, to receiving test ad for testing purpose
     autoShow: true // auto show interstitial ad when loaded, set to false if prepare/show
  });
  // new events, with variable to differentiate: adNetwork, adType, adEvent
  $(document).on('onAdFailLoad', function(data){
    console.log('error: ' + data.error +
        ', reason: ' + data.reason +
        ', adNetwork:' + data.adNetwork +
        ', adType:' + data.adType +
        ', adEvent:' + data.adEvent); // adType: 'banner', 'interstitial', etc.
  });
  $(document).on('onAdLoaded', function(data){});
  $(document).on('onAdPresent', function(data){});
  $(document).on('onAdLeaveApp', function(data){});
  $(document).on('onAdDismiss', function(data){});

  $('#btn_create').click(createSelectedBanner);
  $('#btn_remove').click(function(){
    AdMob.removeBanner();
  });
  $('#btn_show').click(showBannerAtPosition);
  $('#btn_hide').click(function(){
    AdMob.hideBanner();
  });

  // test interstitial ad
  $('#btn_prepare').click(function(){
    AdMob.prepareInterstitial({
      adId:admobid.interstitial,
      autoShow: false,
    });
  });
  $('#btn_showfull').click(function(){
    AdMob.showInterstitial();
  });

    createSelectedBanner();
    showBannerAtPosition();

    // test case for #256, https://github.com/floatinghotpot/cordova-admob-pro/issues/256
    $(document).on('backbutton', function(){
        var state=HexGame.BootState.STATE_NOW;
        if (state=='GAME_STATE'){
            HexGame.game.state.start('Home');  
        }
        else if (state=='GAME_STATE_2') 
        {
            if(window.confirm('Если вы выйдете из игры то потеряете текущий прогресс. Продолжить?')) HexGame.game.state.start('Home2');  
        }
        else if (state=='GAME_RETURN') HexGame.game.state.start('Game',true, false, {"save":true, "type":1 });
        else if (state=='HELP') HexGame.game.state.start('Menu');
        else  if (state=='HOME') HexGame.game.state.start('Menu'); 
         else  if (state=='HOME2') HexGame.game.state.start('Menu'); 
        else  if (state=='SETTING') HexGame.game.state.start('Menu'); 
        else  if (state=='SHOP') HexGame.game.state.start('Menu'); 
        else  if (state=='SELECT_GAME') HexGame.game.state.start('Menu'); 
        else  if (state=='MENU')  if(window.confirm('Вы действительно хотите выйти?')) navigator.app.exitApp();
    });


    $(".back").click(function(){
        // else if (state=='GAME_RETURN') HexGame.game.state.start('Game',true, false, {"save":true, "type":1 });
        var state=HexGame.BootState.STATE_NOW;
        if (state=='GAME_STATE') 
        {
            HexGame.game.state.start('Home');  
        }
        else if (state=='GAME_STATE_2') 
        {   
            console.log('Если вы выйдете из игры то потеряете текущий прогресс. Продолжить?'); HexGame.game.state.start('Home2'); 
            //if(window.confirm('Если вы выйдете из игры то потеряете текущий прогресс. Продолжить?')) HexGame.game.state.start('Home2');  
        }
        else if (state=='GAME_RETURN') HexGame.game.state.start('Game',true, false, {"save":true, "type":1 });
        else  if (state=='HELP') HexGame.game.state.start('Menu');
        else  if (state=='HOME') HexGame.game.state.start('Menu');
        else  if (state=='HOME2')  HexGame.game.state.start('Menu');  
        else  if (state=='SETTING') HexGame.game.state.start('Menu'); 
        else  if (state=='SHOP') HexGame.game.state.start('Menu'); 
        else  if (state=='SELECT_GAME') HexGame.game.state.start('Menu'); 
        else  if (state=='MENU')  if(window.confirm('Вы действительно хотите выйти?')) navigator.app.exitApp();
    });
  // test case #283, https://github.com/floatinghotpot/cordova-admob-pro/issues/283
  $(document).on('resume', function(){

    AdMob.showInterstitial();
  });
}

function initAdMob() {
    if (! AdMob) { console.log( 'admob plugin not ready' ); return; }

    initAd();
    createSelectedBanner();
}
var HexGame = HexGame || {};
HexGame.Shop = {
init: function(param) {
    this.param=param || { "back":false, "state":"SHOP", "type":1, 'save':true };
},
create: function() {
    HexGame.BootState.STATE_NOW=this.param.state;
    this.ICON_ZOOM=0.5;
    this.ZOOM=HexGame.BootState.ZOOM;
    this.CLICK=HexGame.BootState.CLICK;
    this.itemUserStat=HexGame.BootState.itemUserStat;

    this.GUI = new mini.GUI(this);  

    this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
    this.game.world.setBounds(0, 0, this.game.width, this.game.height);
    this.game.world.sendToBack(this.background);

    this.menuGroup = this.add.group();


    this.topGroup = this.add.group();
    //--------
    this.iconBack = this.GUI.iconCreate({'x':this.game.width - 25*this.ZOOM, 'y':25*this.ZOOM, 'frameName':'icon/list.png', 'scale':this.ICON_ZOOM*0.7});
    this.iconBack.events.onInputDown.add(function(){
        if (this.param.back) this.state.start('Game', true, false,{"save":true, "type":this.param.type}); 
        else this.state.start('Menu'); 
    }, this);	
    this.topGroup.add(this.iconBack);
    //---------

    var styleText= {font: 28*this.ZOOM+'px Arial', fill: '#fff'},
    styleText2= {font: 18*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"};

    this.labelText = this.game.add.text(25,25*this.ZOOM, 'Подсказки', styleText);
    this.labelText1 = this.game.add.text(25,this.labelText.y+40*this.ZOOM, 'Мой баланс: '+this.itemUserStat.coin+'$', styleText2);
    //this.labelText.x=this.game.width-this.labelText.width;


    this.iconMenu1= this.GUI.iconCreate({'x':this.game.width*0.15, 'y': this.labelText1.y+100*this.ZOOM, 'frameName':'icon/light-bulb.png'});

    this.iconMenu1.coinText = this.game.add.text(this.iconMenu1.right-20*this.ZOOM,this.iconMenu1.bottom-30*this.ZOOM, '0', styleText);
    this.GUI.textShadow(this.iconMenu1.coinText,  { "stroke": "#0f4856","strokeThickness":10, "shadow":true });

    this.menu1 = this.game.add.text(this.iconMenu1.right+20*this.ZOOM,this.iconMenu1.top, 'Нахождение пары', styleText2);
    this.menu12 = this.game.add.text(this.iconMenu1.right+20*this.ZOOM,this.menu1.bottom+5*this.ZOOM, 'Цена: 50$', styleText2);   


    this.menu1Buy= this.GUI.iconCreate({'x':this.game.width*0.85, 'y': this.iconMenu1.y, 'frameName':'icon/basket.png'});
    this.menu1Buy.data={ "item":1, "price":50 };
    this.menu1Buy.events.onInputDown.add(this.buyItem, this);

    this.iconMenu2= this.GUI.iconCreate({'x':this.game.width*0.15, 'y': this.iconMenu1.y+80*this.ZOOM, 'frameName':'icon/undo.png'});
    this.iconMenu2.coinText = this.game.add.text(this.iconMenu2.right-20*this.ZOOM,this.iconMenu2.bottom-30*this.ZOOM, '0', styleText);
    this.GUI.textShadow(this.iconMenu2.coinText,  { "stroke": "#0f4856","strokeThickness":10, "shadow":true });

    this.menu2 = this.game.add.text(this.iconMenu2.right+20*this.ZOOM,this.iconMenu2.top, 'Отмена действия', styleText2);
    this.menu22 = this.game.add.text(this.iconMenu2.right+20*this.ZOOM,this.menu2.bottom+5*this.ZOOM, 'Цена: 30$', styleText2);   


    this.menu2Buy =  this.GUI.iconCreate({'x':this.game.width*0.85, 'y': this.iconMenu2.y, 'frameName':'icon/basket.png'}); 
    this.menu2Buy.data={ "item":2, "price":30 };
    this.menu2Buy.events.onInputDown.add(this.buyItem, this);

    this.saveInfo();

 
},

buyItem: function(item){
    if (this.CLICK==1){this.CLICK=0;}
    else {
        this.CLICK++;
        //console.log(this.itemUserStat.coin-item.data.price>=0);
        if (this.itemUserStat.coin-item.data.price>=0){

            this.itemUserStat.coin-=item.data.price;
            this.itemUserStat['tip'+item.data.item]++;
            this.saveInfo();
        }
        else{
            this.GUI.popupMessage('Нет средств',item,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
        }
    }
},

saveInfo: function(){
    this.iconMenu1.coinText.text=this.itemUserStat.tip1;
    this.iconMenu2.coinText.text=this.itemUserStat.tip2;
    this.labelText1.text='Мой баланс: '+this.itemUserStat.coin+'$';
    localStorage.setItem('itemUserStat', JSON.stringify(this.itemUserStat));
}

}
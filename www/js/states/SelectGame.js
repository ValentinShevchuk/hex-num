var HexGame = HexGame || {};
HexGame.SelectGame = {
create: function() {
    HexGame.BootState.STATE_NOW='SELECT_GAME';
    this.ZOOM=HexGame.BootState.ZOOM;
    this.ICON_ZOOM=0.7;
    this.game.world.setBounds(0, 0, this.game.width, this.game.height);

    this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
    this.game.world.sendToBack(this.background);

    var styleText= {font: 22*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-this.game.width*0.3},
    styleText2= {font: 16*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"};



    this.TRAINING=3;
    var nameStorage = 'TRAINING';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
      // 
      this.TRAINING = localStorage.getItem(nameStorage);
      console.log('загрузка TRAINING');
    }

    this.topGroup = this.add.group();
    //--------
    this.iconBack = this.game.add.sprite(this.game.width - 25*this.ZOOM, 25*this.ZOOM,"images" );
    this.iconBack.frameName='icon/list.png';
    this.iconBack.anchor.setTo(0.5);
    this.iconBack.scale.setTo(this.ICON_ZOOM*0.5);
    this.iconBack.inputEnabled =true;
    this.iconBack.events.onInputDown.add(function(){
    this.state.start('Menu'); 
    }, this);	
    this.topGroup.add(this.iconBack);
    //---------



    // this.state.start('Game'); // this.state.start('Inventory'); 
    this.menuGroup = this.add.group();
    
    
/*
    this.menuItem = this.add.text(this.game.width/2,this.game.height/2-(this.game.height/2)*0.2, 'Играть',  {font: '24px Tahoma', fill: '#fff'});
*/

    this.menuItem1 = new Phaser.Sprite(this.game, this.game.width*0.15 ,this.game.height*0.20,"images" );
    this.menuItem1.frameName='icon/play.png';
    this.menuItem1.scale.setTo(this.ICON_ZOOM);
    this.menuItem1.anchor.setTo(0.5);
    this.menuItem1.inputEnabled =true;
    this.menuItem1.events.onInputDown.add(function(){
        this.state.start('Home'); 
    }, this);	

    this.animPlay(this.menuItem1);
    this.menuGroup.add(this.menuItem1);

    this.menuItem1label = this.game.add.text(this.menuItem1.x+50*this.ZOOM,this.menuItem1.y-30*this.ZOOM, 'Классический режим', styleText);
    this.menuItem1label2 = this.game.add.text(this.menuItem1.x+50*this.ZOOM,this.menuItem1label.y+this.menuItem1label.height, 'Классическая игра без ограничения', {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem1.width+this.menuItem1.x)});

    

    this.menuItem2 = new Phaser.Sprite(this.game, this.menuItem1.x ,this.menuItem1label2.y+ this.menuItem1label2.height+75*this.ZOOM,"images" );
    this.menuItem2.frameName='icon/play.png';
    this.menuItem2.scale.setTo(this.ICON_ZOOM);
    this.menuItem2.anchor.setTo(0.5);
    this.menuItem2.inputEnabled =true;
    this.menuItem2.events.onInputDown.add(function(){
        //this.state.start('Game',true,false,{ "save": false, "type":2}); 
        if (this.TRAINING<2) 
        this.state.start('Home2'); 
    }, this);	

    if (this.TRAINING<2) this.animPlay(this.menuItem2);
    else this.menuItem2.alpha=0.4
    this.menuGroup.add(this.menuItem2);

    this.menuItem2label = this.game.add.text(this.menuItem2.x+50*this.ZOOM,this.menuItem2.y-30*this.ZOOM, 'Хардкор режим', styleText);
    this.menuItem2label2 = this.game.add.text(this.menuItem2.x+50*this.ZOOM,this.menuItem2label.y+this.menuItem2label.height, this.TRAINING>1?'Пройдите обучение в классической игре':'Игра на время и без сохранений',  {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem2.width+this.menuItem2.x)});


    this.menuItem3 = new Phaser.Sprite(this.game, this.menuItem1.x ,this.menuItem2label2.y+ this.menuItem2label2.height+75*this.ZOOM,"images" );
    this.menuItem3.frameName='icon/play.png';
    this.menuItem3.scale.setTo(this.ICON_ZOOM);
    this.menuItem3.anchor.setTo(0.5);
    this.menuItem3.inputEnabled =true;
    this.menuItem3.events.onInputDown.add(function(){
        //this.state.start('HomeGame2'); 
    }, this);	

    //this.animPlay(this.menuItem3);
    this.menuGroup.add(this.menuItem3);

    this.menuItem3label = this.game.add.text(this.menuItem3.x+50*this.ZOOM,this.menuItem3.y-30*this.ZOOM, 'Кампания (скоро)', styleText);
    this.menuItem3label2 = this.game.add.text(this.menuItem3.x+50*this.ZOOM,this.menuItem3label.y+this.menuItem3label.height, '',  {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem3.width+this.menuItem3.x)});


    //this.menuItem2.alpha=0.4;
    this.menuItem3.alpha=0.4;
/*
    this.itemSound = new Phaser.Sprite(this.game, (this.game.width)*0.70,this.menuItem.y+100*this.ZOOM,"images" );
    this.itemSound.frameName='icon/gear.png';
    this.itemSound.scale.setTo(0.75*this.ZOOM);
    this.itemSound.anchor.setTo(0.5);

    this.itemSound.inputEnabled =true;
    this.itemSound.events.onInputDown.add(function(){
        this.state.start('Setting');
    }, this);	
    this.menuGroup.add(this.itemSound );

    this.itemHelp = new Phaser.Sprite(this.game, (this.game.width)*0.30,this.menuItem.y+100*this.ZOOM,"images" );
    this.itemHelp.frameName='icon/info.png';
    this.itemHelp.scale.setTo(0.75*this.ZOOM);
    this.itemHelp.anchor.setTo(0.5);
    this.itemHelp.inputEnabled =true;
    this.itemHelp.events.onInputDown.add(function(){
        this.state.start('Help');
    }, this);	
    this.menuGroup.add(this.itemHelp );

*/
    
/*
    this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+150, 'Как играть?',  {font: '24px Tahoma', fill:'#fff'});
    this.menuItem.anchor.setTo(0.5);   
    this.menuItem.inputEnabled =true;
    this.menuItem.events.onInputDown.add(function(){         
        this.state.start('Game',true,false,{ save: true}); 
    }, this);


    this.menuGroup.add(this.menuItem );*/

//this.state.start('Game');   

},
update: function(){
    
},
animPlay:function(obj,ran){
        var bounce=this.game.add.tween(obj);
        var ran=ran*-1||10;
    bounce.to({ width:obj.width+ran,height: obj.width+ran,  }, 1000 + Math.random() * 3000, Phaser.Easing.Bounce.In);
    bounce.onComplete.add(function(){

        //	obj.width=temp; obj.height=temp;
            this.animPlay(obj, ran);
        }, this);
    bounce.start();
}
};
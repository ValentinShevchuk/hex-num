var HexGame = HexGame || {};
HexGame.HomeState = {
create: function() {
    this.ZOOM=HexGame.BootState.ZOOM;
    this.ICON_ZOOM=0.5;
    HexGame.BootState.STATE_NOW='HOME';
    this.GUI = new mini.GUI(this); 

    //this.scale.setGameSize(window.innerWidth*0.5, window.innerHeight*0.5);
    this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
    this.game.world.sendToBack(this.background);

     this.homeGroup = this.add.group();

    this.menuIcon= this.GUI.iconCreate({'x':this.game.width - 25*this.ZOOM, 'y':25*this.ZOOM, 'frameName':'icon/list.png', 'scale':0.7*this.ICON_ZOOM});
    this.menuIcon.events.onInputDown.add(  function(){
        this.state.start('Menu');
    }, this);
    this.homeGroup.add(this.menuIcon );

    // this.state.start('Game'); // this.state.start('Inventory'); 
   
    this.menuItem = this.add.text(this.game.width/2,this.game.height/4, 'Начать игру',  {font: this.ZOOM*24+'px Arial', fill: '#fff'});
    this.menuItem.anchor.setTo(0.5);
    this.menuItem.inputEnabled =true;
    this.menuItem.events.onInputDown.add(function(){
        localStorage.removeItem('save');
        localStorage.removeItem('userStat');
        this.state.start('Game',true,false,{ save: false}); 
    }, this);	
    this.homeGroup.add(this.menuItem );
    var block=(localStorage.getItem('save') != null && localStorage.getItem('save') != undefined)?false:true;

    this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+50*this.ZOOM, 'Продолжить игру',  {font: this.ZOOM*24+'px Arial', fill:(block)?'#7f7f7f' :'#fff'});
    this.menuItem.anchor.setTo(0.5);   
    if (!block){  
            this.menuItem.inputEnabled =true;
            this.menuItem.events.onInputDown.add(function(){         
                    this.state.start('Game',true,false,{ save: true}); 
            }, this);	
    }
    this.homeGroup.add(this.menuItem );


    this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+50*this.ZOOM, 'Правила игры',  {font: this.ZOOM*20+'px Arial', fill:'#ddd'});
    this.menuItem.anchor.setTo(0.5);   

            this.menuItem.inputEnabled =true;
            this.menuItem.events.onInputDown.add(function(){         
                    this.state.start('Help'); 
            }, this);	
    
    this.homeGroup.add(this.menuItem );



    this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+50*this.ZOOM, 'Купить подсказки',  {font: this.ZOOM*20+'px Arial', fill:'#ddd'});
    this.menuItem.anchor.setTo(0.5);   

        this.menuItem.inputEnabled =true;
        this.menuItem.events.onInputDown.add(function(){         
                this.state.start('Shop'); 
        }, this);	
    
    this.homeGroup.add(this.menuItem );
    //this.state.start('Game');   
},
update: function(){

},
};
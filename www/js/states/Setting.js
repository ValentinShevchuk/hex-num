var HexGame = HexGame || {};
HexGame.Setting = {
create: function() {
    this.ZOOM_ICON=0.5;
    HexGame.BootState.STATE_NOW='SETTING';
    this.ZOOM=HexGame.BootState.ZOOM;
    this.CLICK=HexGame.BootState.CLICK;
    //this.scale.setGameSize(window.innerWidth*1, window.innerHeight*1);
    this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
    //this.background.autoScroll(1, 1);

    this.game.world.sendToBack(this.background);
    // this.state.start('Game'); // this.state.start('Inventory'); 
    this.menuGroup = this.add.group();


    this.topGroup = this.add.group();
    //--------
    this.iconBack = this.game.add.sprite(this.game.width - 25*this.ZOOM, 25*this.ZOOM,"images" );
    this.iconBack.frameName='icon/list.png';
    this.iconBack.anchor.setTo(0.5);
    this.iconBack.scale.setTo(this.ZOOM_ICON*0.7);
    this.iconBack.inputEnabled =true;
    this.iconBack.events.onInputDown.add(function(){
    this.state.start('Menu'); 
    }, this);	
    this.topGroup.add(this.iconBack);
    //---------

    var styleText= {font: 28*this.ZOOM+'px Arial', fill: '#fff'},
    styleText2= {font: 18*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"};

    this.labelText = this.game.add.text(25,25*this.ZOOM, 'Настройки', styleText);

    this.iconMenu1 = this.game.add.sprite(this.game.width*0.15, 100*this.ZOOM,"images" );
    this.iconMenu1.scale.setTo(0.25*this.ZOOM);
    this.iconMenu1.frameName=HexGame.BootState.ANIMATION==1?'other/button1.png':'other/button0.png';
    this.iconMenu1.anchor.setTo(0.5);
    this.iconMenu1.inputEnabled =true;
    this.iconMenu1.data={ "type":1};
    this.iconMenu1.events.onInputDown.add(this.switch, this);

    this.menu1 = this.game.add.text(this.iconMenu1.right+10,this.iconMenu1.top+10, HexGame.BootState.ANIMATION==1?'Анимация включена':'Анимация отключена', styleText2);

    this.iconMenu2 = this.game.add.sprite(this.iconMenu1.x, this.iconMenu1.bottom+40*this.ZOOM,"images" );
    this.iconMenu2.frameName=HexGame.BootState.SOUND==1?'other/button1.png':'other/button0.png';
    this.iconMenu2.scale.setTo(0.25*this.ZOOM);
    this.iconMenu2.anchor.setTo(0.5);
    this.iconMenu2.inputEnabled =true;
    this.iconMenu2.data={ "type":2};
    this.iconMenu2.events.onInputDown.add(this.switch, this);

    this.menu2 = this.game.add.text(this.iconMenu2.right+10,this.iconMenu2.top+10, HexGame.BootState.SOUND==1?'Звук включен':'Звук выключен', styleText2);


/*
    this.iconMenu3 = this.game.add.sprite(this.iconMenu2.x, this.iconMenu2.bottom+40*this.ZOOM,"images" );
    this.iconMenu3.frameName=HexGame.BootState.ZOOM==1.5?'other/button1.png':'other/button0.png';
    this.iconMenu3.scale.setTo(0.25*this.ZOOM);
    this.iconMenu3.anchor.setTo(0.5);
    this.iconMenu3.inputEnabled =true;
    this.iconMenu3.data={ "type":3};
    this.iconMenu3.events.onInputDown.add(this.switch, this);
    this.menu3 = this.game.add.text(this.iconMenu3.right+10,this.iconMenu3.top+10, HexGame.BootState.ZOOM==1.5?'Высокое разрешение':'Низкое разрешение', styleText2);

*/
    this.iconMenu4 = this.game.add.sprite(this.iconMenu2.x-1000, this.iconMenu2.bottom+40*this.ZOOM,"images" );
    this.iconMenu4.frameName=HexGame.BootState.AUTO_SETTING==1?'other/button1.png':'other/button0.png';
    this.iconMenu4.scale.setTo(0.25*this.ZOOM);
    this.iconMenu4.anchor.setTo(0.5);
    this.iconMenu4.inputEnabled =true;
    this.iconMenu4.data={ "type":4};
    this.iconMenu4.events.onInputDown.add(this.switch, this);
    this.menu4 = this.game.add.text(this.iconMenu4.right+10,this.iconMenu4.top+10, HexGame.BootState.AUTO_SETTING==1?'Автонастройка  включена':'Автонастройка выключена', styleText2);


    this.iconMenu5 = this.game.add.sprite(this.iconMenu2.x, this.iconMenu2.bottom+40*this.ZOOM,"images" );
    this.iconMenu5.frameName='other/button1.png';
    this.iconMenu5.scale.setTo(0.25*this.ZOOM);
    this.iconMenu5.anchor.setTo(0.5);
    this.iconMenu5.inputEnabled =true;
    this.iconMenu5.data={ "type":5};
    this.iconMenu5.events.onInputDown.add(this.switch, this);
    this.menu5 = this.game.add.text(this.iconMenu5.right+10,this.iconMenu5.top+10, 'Сбросить все достижения', styleText2);
/*
	this.menuItem = this.add.text(this.game.width/2,this.game.height/2-(this.game.height/2)*0.2, 'Играть',  {font: '24px Tahoma', fill: '#fff'});
*/
	
/*	
	this.menuItem = new Phaser.Sprite(this.game, this.game.width/2,this.game.height*0.5,"images" );

	this.menuItem.frameName='icon/play.png';
	this.menuItem.scale.setTo(1*this.ZOOM);
	this.menuItem.anchor.setTo(0.5);
	this.menuItem.inputEnabled =true;
	this.menuItem.events.onInputDown.add(function(){
		this.state.start('Home'); 
	}, this);	

	this.animPlay(this.menuItem);
	this.menuGroup.add(this.menuItem );

	this.itemSound = new Phaser.Sprite(this.game, (this.game.width)*0.70,this.menuItem.y+100*this.ZOOM,"images" );
	this.itemSound.frameName=HexGame.BootState.SOUND==1?'icon/sound_on.png':'icon/sound_off.png';
	this.itemSound.scale.setTo(0.75*this.ZOOM);
	this.itemSound.anchor.setTo(0.5);

	this.itemSound.inputEnabled =true;
	this.itemSound.events.onInputDown.add(function(){
		console.log(this.itemSound.frameName);
		this.itemSound.frameName=this.itemSound.frameName=='icon/sound_on.png'?'icon/sound_off.png':'icon/sound_on.png';
		HexGame.BootState.SOUND=this.itemSound.frameName=='icon/sound_on.png'?1:0;
	}, this);	
	this.menuGroup.add(this.itemSound );

	this.itemHelp = new Phaser.Sprite(this.game, (this.game.width)*0.30,this.menuItem.y+100*this.ZOOM,"images" );
	this.itemHelp.frameName='icon/info.png';
	this.itemHelp.scale.setTo(0.75*this.ZOOM);
	this.itemHelp.anchor.setTo(0.5);
	this.itemHelp.inputEnabled =true;
	this.itemHelp.events.onInputDown.add(function(){
		this.state.start('Help');
	}, this);	
	this.menuGroup.add(this.itemHelp );

*/
	
/*
	this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+150, 'Как играть?',  {font: '24px Tahoma', fill:'#fff'});
	this.menuItem.anchor.setTo(0.5);   
	this.menuItem.inputEnabled =true;
	this.menuItem.events.onInputDown.add(function(){         
		this.state.start('Game',true,false,{ save: true}); 
	}, this);	


	this.menuGroup.add(this.menuItem );*/
/*
	this.label = this.state.game.add.text(10, 10, 'v.0.3', {font: '12px Arial', fill: '#fff'});    

	this.label.anchor.setTo(0.5); 

	console.log(this.label.x);
	this.label.x=this.game.width-this.label.width/2;  
	this.label.y=0+this.label.height/2;  
*/
//this.state.start('Game');   

//console.log(navigator.app);
},
update: function(){
	
},
animPlay:function(obj,ran){
    var bounce=this.game.add.tween(obj);
    var ran=ran*-1||10;
    bounce.to({ width:obj.width+ran,height: obj.width+ran,  }, 1000 + Math.random() * 3000, Phaser.Easing.Bounce.In);
    bounce.onComplete.add(function(){

    //	obj.width=temp; obj.height=temp;
        this.animPlay(obj, ran);
    }, this);
    bounce.start();
},
switch: function(item){
    if (this.CLICK==1){this.CLICK=0;}
    else {
        this.CLICK++;

        if (item.data.type!=5){
            if (item.frameName=='other/button1.png'){
                item.frameName='other/button0.png';
            }else{
                item.frameName='other/button1.png';
            }
        }
        if (item.data.type==1){
            if (item.frameName=='other/button1.png')
            {
                HexGame.BootState.ANIMATION=1;
                this.menu1.text='Анимация включена';
            }
            else
            {
                HexGame.BootState.ANIMATION=0;
                this.menu1.text='Анимация отключена';
            }
        
        } 

        else if (item.data.type==2){
            if (item.frameName=='other/button1.png')
            {
                HexGame.BootState.SOUND=1;
                this.menu2.text='Звук включен';
            }
            else
            {
                HexGame.BootState.SOUND=0;
                this.menu2.text='Звук выключен';
            }
        }
        else if (item.data.type==3){
            
             HexGame.GameState.GUI.popupMessage('Изменения вступят в силу после перезагрузки игры',this,{  style:{ fill: '#444', font:14*HexGame.BootState.ZOOM+'px Arial', align: 'center'}});
            if (item.frameName=='other/button1.png')
            {
                
                // zoom умышлено записывается не в константу чтобы оно не менялось прямо сейчас, иначе сбивается масштаб
                this.ZOOM=1.5;
                this.menu3.text='Высокое разрешение';
            }
            else
            {
                this.ZOOM=1;
                this.menu3.text='Низкое разрешение';
            }
            
        }
        else if (item.data.type==4){
            if (item.frameName=='other/button1.png')
            {
                HexGame.BootState.AUTO_SETTING=1;
                this.menu4.text='Автонастройка включена';
            }
            else
            {
                HexGame.BootState.AUTO_SETTING=0;
                this.menu4.text='Автонастройка выключена';
            }
        }
        else if (item.data.type==5){
             if(window.confirm('Вы действительно хотите начать сначала?')){
                 localStorage.clear();
                 this.state.start('Boot'); 
             }
        }

        var setting={
            "ANIMATION":HexGame.BootState.ANIMATION,
            "SOUND":HexGame.BootState.SOUND,
            "ZOOM":this.ZOOM,
            "AUTO_SETTING":HexGame.BootState.AUTO_SETTING
            
        };
        localStorage.setItem('setting', JSON.stringify(setting));	
    }
}

}
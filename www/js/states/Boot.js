var HexGame = HexGame || {};

//setting game configuration and loading the assets for the loading screen
HexGame.BootState = {
  init: function() {
    //loading screen will have a white background
    this.game.stage.backgroundColor = '#ffcc33';

    //scaling options
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

    //have the game centered horizontally
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
  },
  preload: function() {
    //assets we'll use in the loading screen
    this.load.image('bar', 'assets/images/preloader-bar.png');
    this.load.image('background', 'assets/images/background.png');
  },
  create: function() {

        this.TRAINING=3;
        this.SOUND=1;
        this.ANIMATION=1;
        this.ZOOM=HexGame.game.ZOOM;
        this.AUTO_SETTING=0;
        this.CLICK=2;

        var nameStorage = 'TRAINING', setting={};
        if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
            // 
            this.TRAINING = localStorage.getItem(nameStorage);
    
            console.log('загрузка обучения');
        }


        var nameStorage = 'setting', setting={};
        if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
            // 
            setting = JSON.parse(localStorage.getItem(nameStorage));
            this.SOUND=setting.SOUND;
            this.ANIMATION=setting.ANIMATION;
            this.AUTO_SETTING=setting.AUTO_SETTING;
            console.log('загрузка настроек...');
        }

        this.itemUserStat={
            "coin":100,
            "tip1":2,
            "tip2":2
        };
        var nameStorage = 'itemUserStat';
        if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
            this.itemUserStat = JSON.parse(localStorage.getItem(nameStorage));
            console.log('загрузка данных  денег и подсказок...');
        }


    this.STATE_NOW='BOOT';
    this.state.start('Preload');
  }
};
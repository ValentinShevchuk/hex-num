var HexGame = HexGame || {};
HexGame.GameState = {
    preload: function() {
    this.game.physics.arcade.isPaused = false;
},
init: function(param) {

    this.game.time.advancedTiming = true;
    this.ZOOM=HexGame.BootState.ZOOM;
    HexGame.BootState.STATE_NOW='GAME_STATE';
    this.param=param||{ "save": false, "type":1};

    this.hardcoreTry=0;
    this.campTry=0;
    if (this.param.type==2)  HexGame.BootState.STATE_NOW='GAME_STATE_2';

    //console.log(HexGame.BootState.STATE_NOW);

    this.INDEX=1;
    this.TILE_W = 56*this.ZOOM;
    this.TILE_H = 64*this.ZOOM;

    this.WIDTH=HexGame.game.DIM.w;
    this.HEIGHT=HexGame.game.DIM.h;	

    this.TRAINING=3;
    var nameStorage = 'TRAINING';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    this.TRAINING = localStorage.getItem(nameStorage);
    console.log('загрузка TRAINING');
    }

    if (this.TRAINING>1 ){

        this.TILE_COL=1+(4-this.TRAINING);
        this.TILE_ROW=1+(4- this.TRAINING);
    }
    else{
        this.TILE_COL=	5; 
        this.TILE_ROW=8;
    }
    this.TILE_COUNT=this.TILE_COL*this.TILE_ROW;
    this.MARGIN_X = ((this.WIDTH- this.TILE_COL*this.TILE_W)/2)- this.TILE_W /4;//50*this.ZOOM;//((this.WIDTH- this.TILE_COL*this.TILE_W)*0.5);
    this.MARGIN_Y = 50*this.ZOOM;
    this.UNDO=[];
    this.LAST_BOARD=[];
    this.ICON_ZOOM=0.5;
    this.COIN=0;
    this.PLUS_TIME=5;
    this.saveTimer = this.game.time.create(false);
    this.saveTimer.start();

    this.userStat={
        "clickAll":0, // всего кликов учитывается все клики по блокам
        "destroyCol":0, // блок удален
        "destroyRow":0, // строка разрушена
        "destroyUniq":0, // разрушены все цифры
        "timeInGame":0, // в игре минут
        "coin":0
    };

    this.foreverStat={
        "clickAll":0, // всего кликов учитывается все клики по блокам
        "destroyCol":0, // блок удален
        "destroyRow":0, // строка разрушена
        "destroyUniq":0, // разрушены все цифры
        "timeInGame":0 // в игре минут
    };

    this.itemUserStat=HexGame.BootState.itemUserStat;
    this.timerGame();

    this.addBoardCount=HexGame.BootState.CLICK;
    this.clickHexCounter=this.addBoardCount;

    // хардкор уровень по умолчанию выключено
    this.hardcore=false;
    this.CAMP=false;

    if (this.param.type==2){
        this.hardcore=true;
        this.hardcoreStat={
            "level":1,
            "stat":[
            /* "eff":0,
                "count":0,
                "time":0 */
            ]
        };
    }
    else if (this.param.type==3)
    {
        this.CAMP=true;
        this.campStat={
            "level":1,
            "stat":[
            /* "eff":0,
                "count":0,
                "time":0 */
            ]
        };
    }
    this.save=undefined;
    //выгрузка сохраненок
    this.loadData(); 
    // если ничего не выгружено, то создается поле
    if (this.save==undefined || !this.param.save) { 
        this.save=this.randomBoard();
        console.log('выгружено из файла');
    } else {
        console.log('выгружено из кэша');
    }

    // если хардкор, задать таймер 
    if (this.hardcore) 
    {
        this.userStat.timeInGame=60;//-this.hardcoreStat.level;
        this.hardcoreTime=0;
        this.hardcoreTry++;
    }

    // подгрузка музыки
    this.soundClick=this.game.add.audio('destroy');
    this.soundWin=this.game.add.audio('win');
    this.soundBoom=this.game.add.audio('boom');
    //	this.clearStorage();//	
    // контроль за полетом, если 1 = можно лететь
    this.FLY_TIPS=1;
    // кол-во пойманых монеток
    this.COUNT_TIPS=0;

    // двойное добавление поля - проверяется в функции
    this.CHEK_ADD_HEX=0;

},
timerGame: function(){
    this.saveTimer.add(Phaser.Timer.SECOND , function () 
    {
        if (this.hardcore)
        { 
            this.userStat.timeInGame--;
                this.hardcoreTime++;

        }
        else this.userStat.timeInGame++;

        if (this.userStat.timeInGame%60==0) this.saveData();

        var min=Math.floor(this.userStat.timeInGame/60),
        sec=this.userStat.timeInGame%60<10?0+''+this.userStat.timeInGame%60:this.userStat.timeInGame%60;

        this.labelTime.text=min+':'+sec;
        /*
        if (this.userStat.coin!=this.labelCoin.text){ 
        var tempCoin=this.COIN>1000?Math.floor(this.COIN/100)+'K':this.COIN;
        this.labelCoin.text="$"+tempCoin;
        }
        */
        //console.log(!this.hardcore,this.userStat.timeInGame<1);
        if (this.hardcore && this.userStat.timeInGame<1){
            //this.state.start('Home2');
            this.lose();
        } 
        this.timerGame();
    // console.log(this.userStat.timeInGame%((rand(0,9)*10))==0);
        if (this.TRAINING<=1) 
        if (!this.hardcore && this.COUNT_TIPS<10)
        if (this.FLY_TIPS>0 && rand(0,20)==5)
        {
            if (this.COUNT_TIPS>3) this.goFlyTips(0);
            else 
            { 
                if (rand(0,5)) this.goFlyTips(0); 
                else this.goFlyTips(rand(0,2)); 
            }
            this.FLY_TIPS--;
        }
    },this);
},
goFlyTips: function(tip){
    var typeTip=[
        {
            "asset":"icon/dollar.png",

        },
        {
            "asset":"icon/light-bulb.png",
        },
        {
            "asset":"icon/undo.png",
        }
    ];
    var direct=rand(1,4),
        flyStart={"x":0, "y":0},
        flyEnd={"x": this.game.width+50, "y":this.game.height+50},
        width=this.game.world.width, 
        height=this.game.world.height, 
        dist=0;

    if (direct==1)
    {
        //сверху
        flyStart={"x": rand(0, width), "y": rand(-50,-100)};
        flyEnd={"x": rand(0, width), "y": height+100};
    }
    else if (direct==2)
    {
        //справа
        flyStart={"x": width+rand(50,100), "y": rand(0,height)};
        flyEnd={"x": rand(-100, -50), "y":rand(0,height)};

    }
    else if (direct==3)
    {
        //снизу
        flyStart={"x": rand(0, width), "y": rand(height+50,height+100)};
        flyEnd={"x": rand(0, width), "y": rand(-100, -50)};
    }
    else if (direct==4)
    {
    //слева
        flyStart={"x":rand(-100, -50), "y": rand(0,height)};
        flyEnd={"x": width+rand(50,100) , "y":rand(0,height)};
    }


    dist=Math.sqrt(Math.abs(Math.pow(flyEnd.x-flyStart.x,2)+Math.pow(flyEnd.y-flyStart.y,2)));

    this.flyTip = this.add.sprite(flyStart.x, flyStart.y, 'images');
    this.flyTip.frameName=typeTip[tip].asset;
    this.flyTip.anchor.setTo(0.5);
    this.flyTip.inputEnabled = true;
    this.flyTip.scale.setTo(this.ICON_ZOOM);
    this.flyTip.events.onInputDown.add(function(){
        if (this.addBoardCount==1)
        {
            this.addBoardCount=0;
        }
        else 
        {
            this.addBoardCount++;

            if (tip>0){
                this.itemUserStat['tip'+tip]++;
                this['tip'+tip].text=this.itemUserStat['tip'+tip];
            }
            else
            {
                this.itemUserStat.coin++;
            }
            this.flyTipTween.pause();

            this.flyTipTween3=this.game.add.tween(this.flyTip).to({ alpha:0}, 1000, Phaser.Easing.Cubic.In, true , 0);
            this.flyTipTween3.onComplete.add(
                function()
                {
                    if (this.FLY_TIPS==0) this.FLY_TIPS++;
                    //this.flyTip.kill();
                    this.flyTip.destroy();
                }
            , this);

            this.saveData();


            console.log('Ура');
        // this.FLY_TIPS=0;
            this.COUNT_TIPS++;
        }
    }, this);
    this.flyTipTween=this.game.add.tween(this.flyTip).to(flyEnd, dist*4.5, Phaser.Easing.Cubic.In, true , 0);
    this.flyTipTween.onComplete.add(
        function()
        {
            if (this.FLY_TIPS==0) this.FLY_TIPS++;
            this.flyTip.destroy();
        }
    , this);
    this.flyTipTween2=this.game.add.tween(this.flyTip).to({"angle": 360}, dist*4.5, Phaser.Easing.Linear.None, true , 0);
    // this.flyTipTween3=this.game.add.tween(this.flyTip.scale).to({"x":-1, "y":1}, dist*5, Phaser.Easing.Linear.None, true , 0);
},
loadData: function () 
{
    if (!this.hardcore){}
    var nameStorage = 'save';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    this.save = JSON.parse(localStorage.getItem(nameStorage));
    console.log('загрузка данных позиции...');
    }


    var nameStorage = 'userStat';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    this.userStat = JSON.parse(localStorage.getItem(nameStorage));
    if (!this.hardcore) this.COIN=this.userStat.coin;
    if (!this.param.save) this.userStat.timeInGame=0;
    console.log('загрузка статистики игрока');
    }

    var nameStorage = 'foreverStat';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    this.foreverStat = JSON.parse(localStorage.getItem(nameStorage));
    console.log('загрузка постоянной статистики игрока');
    }


    var nameStorage = 'hardcoreStat';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) 
    {
        this.hardcoreStat = JSON.parse(localStorage.getItem(nameStorage));
        console.log('загрузка данных статистики...');
    }


    var nameStorage = 'campStat';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) 
    {
        this.campStat = JSON.parse(localStorage.getItem(nameStorage));
        console.log('загрузка данных статистики компании...');
    }

    var nameStorage = 'hardcoreTry';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
    // 
    this.hardcoreTry = JSON.parse(localStorage.getItem(nameStorage));
    console.log('загрузка данных статистики...');
    }

},
saveData: function () 
{

    console.log('сохранение...');

    var temp=[];
    this.userStat.coin=this.COIN;
    if (this.LAST_BOARD && !this.hardcore) localStorage.setItem('save', JSON.stringify(this.LAST_BOARD));	
    if (this.userStat) localStorage.setItem('userStat', JSON.stringify(this.userStat));
    if (this.foreverStat) localStorage.setItem('foreverStat', JSON.stringify(this.foreverStat));
    if (this.hardcore) localStorage.setItem('hardcoreStat', JSON.stringify(this.hardcoreStat));
    if (this.itemUserStat) localStorage.setItem('itemUserStat', JSON.stringify(this.itemUserStat));
    if (this.TRAINING) localStorage.setItem('TRAINING', this.TRAINING);
    if (this.hardcoreTry) localStorage.setItem('hardcoreTry', this.hardcoreTry);
    if (this.campStat) localStorage.setItem('campStat', JSON.stringify(this.campStat));
    
},

create: function() {

    this.background = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.height, 'background2');
    //this.background.scale.setTo(this.ZOOM);

    this.game.world.sendToBack(this.background);
    this.background.fixedToCamera = true; 


    this.initGUI();

    this.board=undefined;

    this.LAST_BOARD=this.save;
    //this.win();
    this.refreshBoard2(this, this.save);

    if (this.TRAINING>1)
    {
        this.GUI.popupMessage('Режим обучения\nВыбирайте одинаковые цифры',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
    }

    this.kineticScrolling = this.game.plugins.add(Phaser.Plugin.KineticScrolling);
    this.kineticScrolling.configure(
        {
            verticalScroll: true,
            horizontalScroll: true
        }
    );
    this.kineticScrolling.start();
},


  clearSelection: function() {

    this.board.forEachAlive(function(data){
      data.tint=0xffffff;
      data.alpha=1;
      data.click=0;
    });

  //	this.board.setAll('alpha', 1);
  //	this.board.setAll('numLabel.fill', '#444');
    //numLabel.fill='#fff';
    //	this.board.setAll('click', 0);

    //remove attached events from tiles
    /*  this.board.forEach(function(tile){
        tile.events.onInputDown.removeAll();
      }, this); */
  },

  //shuffle array method from http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript)
  shuffle: function(array) {
    var counter = array.length, temp, index;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
  },
  textShadow:function(text, style){
      var style=style || {};
    //	console.log(style);
      style.shadow=style.shadow||false;
      text.stroke=style.stroke || "#de77ae";
      text.strokeThickness = style.strokeThickness || 8;
      if (style.shadow) text.setShadow(2, 2, "#333333", 2, true, false);

  },
  // проверка кол-ва активных строк
    checkCountRow: function()
    {
        var Max=0;
        this.board.forEachAlive(
            function(data)
            {
                if (data.row>Max) Max=data.row;
            }
        , this);
        return Max;

    },
    initGUI: function () {
        this.GUI = new mini.GUI(this);  

        var styleText= {font: 28*this.ZOOM+'px Arial', fill: '#fff'};
        this.panel = this.add.group();

        this.icon = this.GUI.iconCreate({'x':this.game.width*0.1, 'y':this.game.height, 'frameName':'icon/add.png', 'scale':0.4*this.ZOOM});
        this.icon.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else {
                this.addBoardCount++;
                if (this.board.total>0)
                {
                    if ( this.board.total>100) 
                    {
                        this.GUI.popupMessage('Нельзя добавить пока есть возможность хода',this,{  style:{ fill: '#444', font:16*this.ZOOM+'px Arial', align: 'center'}});
                    }
                    else this.addBoard2();
                }
            }
        }, this);
        this.panel.add(this.icon);


        /*
            //icon удаления
            this.iconDel = this.add.sprite( this.game.width-25, this.game.height-30, 'images');
            this.iconDel.frameName='icon/del.png';
            this.iconDel.anchor.setTo(0.5);
            this.iconDel.scale.setTo(this.ICON_ZOOM);
            this.iconDel.inputEnabled = true;
            this.iconDel.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else { 
                this.addBoardCount++;
                if (this.userStat.coin-10>0) {
                this.GUI.popupMessage('Выберите цифру для уничтожения\nКликните по ней два раза',this.iconDel,{  style:{ fill: '#444', font:'14px Arial', align: 'center'}});

                this.iconDel.alpha=this.iconDel.alpha==1?0.5:1; 
                } else {
                this.GUI.popupMessage('Нет средств. Нужно 10$',this.iconDel,{  style:{ fill: '#444', font:'14px Arial', align: 'center'}});
                }
            }
            }, this);
            this.panel.add(this.iconDel);
        */

        //icon локатор

        this.iconMenu0= this.GUI.iconCreate({'x':this.game.width*0.9, 'y':this.game.height, 'frameName':'icon/basket.png', 'scale':0.4*this.ZOOM});
        this.iconMenu0.events.onInputDown.add(function()
        {
            if (this.iconMenu0.alpha==1) this.state.start('Shop', true, false, {"back":true, "state":"GAME_RETURN", "type":this.param.type });
            else if (this.hardcore) this.GUI.popupMessage('Нельзя купить подсказку в хардкор режиме',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
        }, this);
        this.panel.add(this.iconMenu0);

        this.iconMenu1= this.GUI.iconCreate({'x':this.iconMenu0.x-this.iconMenu0.width*1.2, 'y':this.game.height, 'frameName':'icon/light-bulb.png', 'scale':0.4*this.ZOOM});
        this.iconMenu1.events.onInputDown.add(function()
        {
            if (this.addBoardCount==1) this.addBoardCount=0;
            else 
            { 
                this.addBoardCount++;
                if (this.board.total>0)
                {
                    if (this.itemUserStat.tip1>0 && this.iconMenu1.alpha==1) 
                    {
                        if (this.board.findPair())
                        {
                            this.GUI.vanishMessage('-1',this,{  style:{ fill: '#fff', font:14*this.ZOOM+'px Arial', align: 'right'}});
                            this.itemUserStat.tip1--; 
                            this.saveData();
                            this.tip1.text= this.itemUserStat.tip1;
                            if (this.hardcore) this.iconMenu1.alpha=0.6;
                        }
                        else
                        {
                            this.GUI.popupMessage('Нет вариантов',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                        }
                    } 
                    else {
                        if (this.iconMenu1.alpha!=1) this.GUI.popupMessage('В хардкоре режиме подсказку можно использовать только один раз',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                        else 
                        {
                            if (!this.hardcore) this.state.start('Shop', true, false, {"back":true, "state":"GAME_RETURN", "type":this.param.type });
                            else this.GUI.popupMessage('Нельзя купить подсказку в хардкор режиме',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                        }
                    }
                }
            }
        }, this);
        this.panel.add(this.iconMenu1);

        var tipStyle={font: 18*this.ZOOM+'px Arial', fill: '#fff'};

        this.tip1 = this.game.add.text(this.iconMenu1.right-20*this.ZOOM,this.iconMenu1.bottom-50*this.ZOOM, this.itemUserStat.tip1, tipStyle);
        this.GUI.textShadow( this.tip1,  { "stroke": "#0f4856","strokeThickness":10, "shadow":true });

        this.panel.add(this.tip1);



        this.iconMenu2= this.GUI.iconCreate({'x':this.iconMenu1.x-this.iconMenu1.width*1.2, 'y':this.game.height, 'frameName':'icon/undo.png', 'scale':0.4*this.ZOOM});
        this.iconMenu2.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else { 
                this.addBoardCount++;
                if (this.itemUserStat.tip2>0  && this.iconMenu2.alpha==1) {
                    if (this.undo()) { 
                    this.itemUserStat.tip2--; 
                    this.GUI.vanishMessage('-1',this,{  style:{ fill: '#fff', font:14*this.ZOOM+'px Arial', align: 'right'}});
                    this.saveData();
                    this.tip2.text= this.itemUserStat.tip2;
                    if (this.hardcore) this.iconMenu2.alpha=0.6;
                    }else this.GUI.popupMessage('Нет возможности отменить',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                } else {
                    if (this.iconMenu2.alpha!=1) this.GUI.popupMessage('В хардкоре режиме подсказку можно использовать только один раз',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                    else 
                    {
                        if (!this.hardcore) this.state.start('Shop', true, false, {"back":true, "state":"GAME_RETURN", "type":this.param.type });
                        else this.GUI.popupMessage('Нельзя купить подсказку в хардкор режиме',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
                    }
                }
            }
        }, this);
        this.panel.add(this.iconMenu2);

        var tipStyle={font: 18*this.ZOOM+'px Arial', fill: '#fff'};

        this.tip2 = this.game.add.text(this.iconMenu2.right-20*this.ZOOM,this.iconMenu2.bottom-50*this.ZOOM, this.itemUserStat.tip2, tipStyle);
        this.GUI.textShadow( this.tip2,  { "stroke": "#0f4856","strokeThickness":10, "shadow":true });

        this.panel.add(this.tip2);

        if (this.hardcore ){
            this.iconMenu0.alpha=0.6
        // this.iconMenu1.x=-1000;
        // this.iconMenu2.x=-1000;
        // this.tip1.x=-1000;
        // this.tip2.x=-1000;
        } 

        this.panel.setAll('y',this.game.height-80*this.ZOOM);

        this.panel.fixedToCamera = true;
        this.game.world.bringToTop(this.panel);


        this.panelTop	= this.add.group();


        this.menuIcon= this.GUI.iconCreate({'x':this.game.width - 25*this.ZOOM, 'y':25*this.ZOOM, 'frameName':'icon/list.png', 'scale':this.ICON_ZOOM*0.7});

        this.menuIcon.events.onInputDown.add(  function(){
        if (this.hardcore) this.state.start('Home2');
        else  this.state.start('Home');
        }, this);
        this.panelTop.add(this.menuIcon);   

        this.label = this.game.add.text(this.game.width*0.5,25*this.ZOOM, '0:00', styleText);
        //this.label.fixedToCamera = true;  
        this.label.anchor.setTo(0.5);
        this.labelTime=this.label;
        this.panelTop.add(this.label);
        this.GUI.textShadow(this.labelTime, { "stroke": "#cc6666"});

        if (this.hardcore) {
            this.labelLevel = this.game.add.text(40*this.ZOOM,35*this.ZOOM, 'Хардкор\nУр: '+this.hardcoreStat.level, {font: 16*this.ZOOM+'px Arial', fill: '#fff'}); 
            this.labelLevel.anchor.setTo(0.5);
            this.GUI.textShadow(this.labelLevel, { "stroke": "#cc6666"});
            this.panelTop.add(this.labelLevel);
        }


        this.panelTop.fixedToCamera = true;
        this.game.world.bringToTop(this.panelTop);

    },
    undo: function(){
        var temp=[];

        if (this.UNDO.length>0) {
        var indexUndo=this.UNDO.length-1;
        temp=this.UNDO[indexUndo];
        this.refreshBoard2(this,temp);
        this.UNDO.splice(indexUndo,1);
        return 1;
        }else {
        console.log('массив пуст');
        return 0;
        }
    },
    undoSet: function(){ // добавить в очередь 
        var a =  JSON.stringify(this.LAST_BOARD);
        a =  JSON.parse(a);
        this.UNDO.push(a);
    },

    refreshBoard2: function(state, grid)
    {
        var temp=[];
        var win=0;

        grid.forEach(function(data,index){
            if (!data.isDestroy) temp.push(data);
        },this);

        if (this.board){ 
            if (this.board.total==0 && this.board!==undefined) { 
                this.win();
                this.clearStorage();
                win=1;
            }
            this.board.forEach(function(data){
                data.numLabel.destroy();
            });
            this.board.removeAll();
            this.board=[];
        }
        if (win==0){
            if (this.param.type==3) this.board = new HexGame.BoardCamp(this, temp);
            else this.board = new HexGame.Board(this, temp);
            this.game.world.bringToTop(this.panel);
            this.game.world.bringToTop(this.panelTop);
        }
    },
    addBoard2: function()
    {
        if (this.CHEK_ADD_HEX<3)
        {
            this.undoSet();
            // вытаскиваем старый
            var temp2=[];
            this.LAST_BOARD.forEach(function(data,index){
                if (data.alive) temp2.push({"num":data.num, "alive":data.alive, 'type': data.type, 'typeLevel':data.typeLevel});
            },this);
            var tempArray=this.LAST_BOARD.concat(temp2);//array_merge(this.map.grid, newArray);
            this.refreshBoard2(this, tempArray);
            this.CHEK_ADD_HEX++;
        } 
        else
        {   
            if(!this.board.findPair({ "check":true})) this.lose(); else  
            this.GUI.popupMessage('Нельзя добавить пока есть возможность хода',this,{  style:{ fill: '#444', font:14*this.ZOOM+'px Arial', align: 'center'}});
        }
    },
    win:function(){

        this.saveTimer.stop();
        this.foreverStat.destroyCol+=this.userStat.destroyCol;
        this.foreverStat.timeInGame+=this.userStat.timeInGame;

        this.COIN=5;
        if (this.hardcore) this.COIN+=this.hardcoreStat.level;

        if (HexGame.BootState.SOUND) this.soundWin.play();

        var style1={font: 36*this.ZOOM+'px Arial', fill: '#fff'};
        var style2={font: 20*this.ZOOM+'px Arial', fill: '#fff'};

        this.label = this.game.add.text(this.game.width/2,this.game.height*0.15, 'Вы выиграли!', style1);
        this.label.fixedToCamera = true;  
        this.label.anchor.setTo(0.5);
        var efficiency=(this.userStat.destroyCol/this.userStat.clickAll);

        this.timeGame=this.hardcore?this.hardcoreTime:this.userStat.timeInGame;

        this.label = this.game.add.text(this.label.x,this.label.y+150*this.ZOOM, 'Ваша статистика:\nКликов всего: '+this.userStat.clickAll+
        '\nЗакрыто блоков: '+this.userStat.destroyCol+
        '\nМинут в игре: '+Math.ceil(this.timeGame)+' сек.'+
        '\nЭффективность: '+Math.round(efficiency*100)+'%'+
        '\nЗаработано: '+Math.round(this.COIN+(this.COIN*efficiency))+'$'
        , style2);


        this.label.fixedToCamera = true;  
        this.label.anchor.setTo(0.5);

        this.menuItem1 = this.game.add.sprite( this.game.width*0.25 ,this.label.y+this.label.height,"images" );
        this.menuItem1.frameName='icon/play.png';
        this.menuItem1.scale.setTo(this.ICON_ZOOM);
        this.menuItem1.anchor.setTo(0.5);
        this.menuItem1.inputEnabled =true;
        this.menuItem1.events.onInputDown.add(function(){
        
            this.state.start('Game',true,false,{ "save": false, "type":this.param.type}); 
    
        }, this);	

        //this.animPlay(this.menuItem1);

        this.menuItem1label = this.game.add.text(this.menuItem1.x,this.menuItem1.bottom+30, this.hardcore?'Продолжить':'Еще раз', style2);
        this.menuItem1label.anchor.setTo(0.5);     



        this.menuItem2 = this.GUI.iconCreate({'x':this.game.width*0.75, 'y':this.label.y+this.label.height, 'frameName':'icon/list.png'});
        this.menuItem2.events.onInputDown.add(function(){
            this.state.start(this.hardcore?'Home2':'Home');
        }, this);	

        //this.animPlay(this.menuItem1);

        this.menuItem2label = this.game.add.text(this.menuItem2.x,this.menuItem2.bottom+30, 'Меню', style2);
        this.menuItem2label.anchor.setTo(0.5);   
        if (this.TRAINING>1) this.TRAINING--;

        if (this.hardcore){

            this.hardcoreStat.level++;
            
            this.hardcoreStat.stat.push({ "eff":Math.round(efficiency*100), "count":Math.round(this.COIN+(this.COIN*efficiency)), "time":this.hardcoreTime, "try": this.hardcoreTry });
            this.hardcoreTry=0;
        }else{

        }
        if (this.panel) { 
        this.panel.setAll('y',-1000);
        this.panelTop.setAll('y',-1000);
        }
        this.userStat=false;
        this.LAST_BOARD=false;

        this.itemUserStat.coin+=Math.round(this.COIN+(this.COIN*efficiency));
        console.log(this.hardcoreTry,' что тут за значенеи');
        this.saveData();
        this.clearStorage();

    },
    lose:function(){
        this.saveTimer.stop();
        this.board.forEach(function(data){
            data.numLabel.kill();
            data.kill();
        });
        //this.boardNum.removeAll();
    // if (HexGame.BootState.SOUND) this.soundWin.play();

        var style1={font: 36*this.ZOOM+'px Arial', fill: '#fff'};
        var style2={font: 20*this.ZOOM+'px Arial', fill: '#fff'};

        this.label = this.game.add.text(this.game.width/2,this.game.height*0.15, 'Вы проиграли!', style1);
        this.label.fixedToCamera = true;  
        this.label.anchor.setTo(0.5);


        this.label = this.game.add.text(this.label.x,this.label.y+50*this.ZOOM, this.hardcore?'Время закончилось':'Нет вариантов после\nболее двух добавлений цифр', {'font': 20*this.ZOOM+'px Arial', "fill": '#fff', 'align':'center'});
        this.label.fixedToCamera = true;  
        this.label.anchor.setTo(0.5);



        this.menuItem1 = this.game.add.sprite( this.game.width*0.25 ,this.label.y+this.label.height+20*this.ZOOM,"images" );
        this.menuItem1.frameName='icon/play.png';
        this.menuItem1.fixedToCamera = true; 
        this.menuItem1.scale.setTo(this.ICON_ZOOM);
        this.menuItem1.anchor.setTo(0.5);
        this.menuItem1.inputEnabled =true;
        this.menuItem1.events.onInputDown.add(function(){
            this.state.start('Game',true,false,{ "save": false, "type":this.param.type}); 
        }, this);	

        //this.animPlay(this.menuItem1);

        this.menuItem1label = this.game.add.text(this.menuItem1.x,this.menuItem1.bottom+30, this.hardcore?'Повторить':'Повторить', style2);
        this.menuItem1label.anchor.setTo(0.5);  
        this.menuItem1label.fixedToCamera = true;    

        this.menuItem2 = this.game.add.sprite( this.game.width*0.75 ,this.label.y+this.label.height+20*this.ZOOM,"images" );
        this.menuItem2.frameName='icon/list.png';
        this.menuItem2.fixedToCamera = true; 
        this.menuItem2.scale.setTo(this.ICON_ZOOM);
        this.menuItem2.anchor.setTo(0.5);
        this.menuItem2.inputEnabled =true;
        this.menuItem2.events.onInputDown.add(function(){
            this.state.start('Menu');
        }, this);	

        //this.animPlay(this.menuItem1);

        this.menuItem2label = this.game.add.text(this.menuItem2.x,this.menuItem2.bottom+30, 'Меню', style2);
        this.menuItem2label.anchor.setTo(0.5);   
        this.menuItem2label.fixedToCamera = true; 
        if (this.panel) { 
        this.panel.setAll('y',-1000);
        this.panelTop.setAll('y',-1000);
        }
        this.userStat=false;
        this.LAST_BOARD=false;

        this.clearStorage();

    },

    randomBoard: function(){
        var scheme=[
            [
                0,0,0,0,0,
                0,0,0,0,0,
                0,0,1,0,0,
                0,0,0,0,0,
                0,0,0,0,0,

            ]
        ];
        var blockType=[
            {},
            { 'num':rand(5,9), 'type':1, 'typeLevel':1}
            ];
        var temp=[];
        // если хардкор
        if (this.hardcore) {
            // если левел игры меньше 5 то поле должно быть уменьшеным, иначе стандартным
            if (this.hardcoreStat.level<=5)this.TILE_COUNT=5*4;
            else if (this.hardcoreStat.level<=10) this.TILE_COUNT=5*8;
            else if (this.hardcoreStat.level*2>this.TILE_COUNT)
            { // если левел*2 больше стандартного кол-ва то коунт увеличевается в двое, надо ихзменить
                this.TILE_COUNT=this.hardcoreStat.level*2;
            }
        }
        if (this.CAMP) this.TILE_COUNT=25;

        // набиваем поле случайными числами
        for(var i=0; i<this.TILE_COUNT; i++){
            var maxRandom=9;
            if (this.TRAINING>1)
            {
                // если тренировка то чисел будет мало
                maxRandom=4-this.TRAINING;
            }
            if (this.hardcore)
            {
                // если хардкор то случайные числа берутся исходя от левела 1-1, 1-2 ... 1-10
                maxRandom=this.hardcoreStat.level;
            } 
            else if (this.CAMP) maxRandom=9;
            temp.push({"num":rand(1,maxRandom), 'alive':true, 'type':0,'typeLevel':0 });
            //temp.push({"num":rand(1,5), 'alive':true, 'type':0,'typeLevel':0 });
        }

        if (this.CAMP)
        { 
            scheme[0].forEach(function(data, index)
            {
                if (data!=0){ temp[index]=blockType[data]; }
            },this);
        }
        temp2=
        [
            {"num":1, "alive":true},{"num":2, "alive":true},{"num":1, "alive":true},{"num":2, "alive":true},{"num":5, "alive":true},
            {"num":1, "alive":true},{"num":3, "alive":true},{"num":4, "alive":true},{"num":5, "alive":true},{"num":5, "alive":true},	
            {"num":1, "alive":true},{"num":2, "alive":true},{"num":6, "alive":true},{"num":7, "alive":true},{"num":5, "alive":true},
            {"num":1, "alive":true},{"num":2, "alive":true},{"num":3, "alive":true},{"num":4, "alive":true},{"num":5, "alive":true}
        ];
        return temp;
    },
    clearStorage: function(){
        // очистка настроек игрового процесса
        localStorage.removeItem('save');
        localStorage.removeItem('userStat');
        localStorage.removeItem('hardcoreTry');
        
    },

    render: function () {
        //отображение fps
        this.game.debug.text(this.time.fps || '--', 0, 10, "#003333");
    }
};
/*


Идентификатор приложения: ca-app-pub-3663897682421444~6108972827
Идентификатор рекламного блока: ca-app-pub-3663897682421444/7585706029

    [{"num":1,"alive":true},{"num":2,"alive":false},{"num":3,"alive":true},{"num":4,"alive":true},{"num":5,"alive":true},{"num":6,"alive":true},{"num":7,"alive":true},{"num":8,"alive":true},{"num":9,"alive":true}],

    [{"num":1,"alive":true},{"num":1,"alive":true},{"num":1,"alive":true},{"num":2,"alive":true},{"num":1,"alive":true},{"num":3,"alive":true},{"num":1,"alive":true},{"num":4,"alive":true},{"num":1,"alive":true}],

    [{"num":5,"alive":true},{"num":1,"alive":true},{"num":6,"alive":true},{"num":1,"alive":true},{"num":7,"alive":true},{"num":1,"alive":true},{"num":8,"alive":true},{"num":1,"alive":true},{"num":9,"alive":true}]
     */
  
var HexGame = HexGame || {};
HexGame.Home2 = {
create: function() {

    HexGame.BootState.STATE_NOW='HOME2';
    this.ZOOM=HexGame.BootState.ZOOM;
    this.ICON_ZOOM=0.5;
    this.background = this.game.add.tileSprite(0, 0, this.game.width,  this.game.height, 'background2');
    this.game.world.sendToBack(this.background);
    this.background.fixedToCamera = true;  

    this.kineticScrolling = this.game.plugins.add(Phaser.Plugin.KineticScrolling);
    this.kineticScrolling.configure({
        verticalScroll: true,
        horizontalScroll: false
    });
    this.kineticScrolling.start();
        

    var styleText= {font: 22*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-this.game.width*0.3},
    styleText2= {font: 16*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"},
    cssPlay= {font: 14*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"}
    ;

    this.hardcoreStat={
            "level":1,
            "stat":[
               /* "eff":0,
                "count":0,
                "time":0 */
            ]
        };

    var nameStorage = 'hardcoreStat';
    if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
      // 
      this.hardcoreStat = JSON.parse(localStorage.getItem(nameStorage));
      console.log('загрузка данных статистики...');
    }



    this.topGroup = this.add.group();
    //--------
    this.iconBack = this.game.add.sprite(this.game.width - 25*this.ZOOM, 25*this.ZOOM,"images" );
    this.iconBack.frameName='icon/list.png';
    this.iconBack.anchor.setTo(0.5);
    this.iconBack.scale.setTo(this.ICON_ZOOM*0.7);
    this.iconBack.inputEnabled =true;
    this.iconBack.fixedToCamera = true; 
    this.iconBack.events.onInputDown.add(function(){
    this.state.start('Menu'); 
    }, this);	
    this.topGroup.add(this.iconBack);
    //---------



    // this.state.start('Game'); // this.state.start('Inventory'); 
    this.menuGroup = this.add.group();
    
    
/*
    this.menuItem = this.add.text(this.game.width/2,this.game.height/2-(this.game.height/2)*0.2, 'Играть',  {font: '24px Tahoma', fill: '#fff'});
*/

    this.color = [
        [
            "0x9999ff",
            "0xffcccc",
            "0xffccff",
            "0xccffff",
            "0xccffcc",
            "0xffffcc",
            "0x99ccff",
            "0x99cc99",
            "0xffff00"],
        [
            "0x66cc99",
            "0xcc6666",
            "0xf9bb4b",
            "0x4da1ae",
            "0x32b7d1",
            "0x36717f",
            "0xe99785",
            "0xd8dabb",			
            "0x3cbda6",
        ]
        ];


 


   /* this.label=this.game.add.text(this.game.width*0.5,50*this.ZOOM, 'Следующий уровень', styleText2);
    this.label.anchor.setTo(0.5);
    this.label1=this.game.add.text(this.label.right+10*this.ZOOM,50*this.ZOOM, this.hardcoreStat.level, styleText);
    this.label1.anchor.setTo(0.5);
    if (this.hardcoreStat.level==1){  this.label.alpha=0; this.label1.alpha=0} */
    //this.menuItem1 = new Phaser.Sprite(this.game, this.game.width*0.5 ,this.label.bottom+40*this.ZOOM,"images" );
    this.menuItem1 = new Phaser.Sprite(this.game, this.game.width*0.5 ,50*this.ZOOM,"images" );
    this.menuItem1.frameName='icon/play.png';
    this.menuItem1.scale.setTo(this.ICON_ZOOM*this.ZOOM);
    this.menuItem1.anchor.setTo(0.5);
    this.menuItem1.inputEnabled =true;
    this.menuItem1.events.onInputDown.add(function(){
        this.state.start('Game', true, false, {"save":false, "type":2}); 
    }, this);	

    this.animPlay(this.menuItem1);
    this.menuGroup.add(this.menuItem1);
        if (this.hardcoreStat.level>1){ 
    this.menuItem1label = this.game.add.text(this.game.width*0.5,this.menuItem1.bottom+20*this.ZOOM, 'Продолжить', cssPlay);
    this.menuItem1label.anchor.setTo(0.5);
        }else{
            this.menuItem1label = this.game.add.text(this.game.width*0.5,this.menuItem1.bottom+20*this.ZOOM, 'Начать игру', styleText);
            this.menuItem1label.anchor.setTo(0.5);        
        }


    var 
        margin=this.game.width/3.5,
        goX=(this.game.width*0.15)+margin/3, 
        goY=this.menuItem1label.bottom+50*this.ZOOM,
        i=0, j=-1;

        
    if (this.hardcoreStat.stat!=undefined && this.hardcoreStat.stat!=null)
    this.hardcoreStat.stat.forEach(function(data){
        i++;
        if (j==8) j=0; else j++;
        this.level = new Phaser.Sprite(this.game, goX ,goY,"images" );
        this.level.frameName='block/color11.png';
        this.level.scale.setTo(0.8*this.ZOOM);
        this.level.anchor.setTo(0.5);
        this.level.inputEnabled =true;
        this.level.tint =this.color[1][j];

        this.level.events.onInputDown.add(function(){
           // this.state.start('Home'); 
        }, this);	

        // this.animPlay(this.this.level);

        this.levelLabel = this.game.add.text(this.level.x,this.level.y, i, {font: 26*this.ZOOM+"px Arial", fill: "#fff"});
        this.levelLabel.anchor.setTo(0.5);
        HexGame.GameState.textShadow(this.levelLabel, {"strokeThickness":-1,  "shadow":true });
        this.levelLabel = this.game.add.text(this.level.x,this.level.bottom+20*this.ZOOM, data.eff+'%\n'+data.count+'$ / '+data.time+'c', {font: 12*this.ZOOM+"px Arial", fill: "#fff", align:"center"});
        this.levelLabel.anchor.setTo(0.5);

        this.menuGroup.add(this.level);

        if (i%3==0)
        {
            goX=(this.game.width*0.15)+margin/3;
            goY+=this.level.height+40*this.ZOOM;
        }
        else
        { 
            goX+=margin; goY;
        }

    },this);

    for (var i=this.hardcoreStat.level; i<=50; i++)
    {
        this.level = new Phaser.Sprite(this.game, goX ,goY,"images" );
        this.level.frameName='block/color11.png';
        this.level.scale.setTo(0.8*this.ZOOM);
        this.level.anchor.setTo(0.5);
        this.level.inputEnabled =true;
        this.level.tint ='0x555555';//this.color[1][(Math.ceil(i))-1];

        this.level.events.onInputDown.add(function(){
           // this.state.start('Home'); 
        }, this);	

    // this.animPlay(this.this.level);

        this.levelLabel = this.game.add.text(this.level.x,this.level.y, i, {font: 26*this.ZOOM+"px Arial", fill: "#fff"});
        this.levelLabel.anchor.setTo(0.5);


        this.menuGroup.add(this.level);

        if (i%3==0)
        {
            goX=(this.game.width*0.15)+margin/3;
            goY+=this.level.height+40*this.ZOOM;
        }
        else
        { 
            goX+=margin; goY;
        }
        
    }
    // console.log( (this.label.height+100)*i);
    var width=50*55;
    //   if (this.hardcoreStat.stat!=undefined && this.hardcoreStat.stat.length>10) width=this.hardcoreStat.stat.length*50;
    this.game.world.setBounds(0, 0, this.game.width, width);

    if (1==2){
        this.menuItem1 = new Phaser.Sprite(this.game, this.game.width*0.15 ,this.game.height*0.20,"images" );
        this.menuItem1.frameName='icon/play.png';
        this.menuItem1.scale.setTo(0.8*this.ZOOM);
        this.menuItem1.anchor.setTo(0.5);
        this.menuItem1.inputEnabled =true;
        this.menuItem1.events.onInputDown.add(function(){
            this.state.start('Home'); 
        }, this);	

        this.animPlay(this.menuItem1);
        this.menuGroup.add(this.menuItem1);

        this.menuItem1label = this.game.add.text(this.menuItem1.x+50*this.ZOOM,this.menuItem1.y-30*this.ZOOM, 'Классический режим', styleText);
        this.menuItem1label2 = this.game.add.text(this.menuItem1.x+50*this.ZOOM,this.menuItem1label.y+this.menuItem1label.height, 'Классическая игра без ограничения', {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem1.width+this.menuItem1.x)});

        

        this.menuItem2 = new Phaser.Sprite(this.game, this.menuItem1.x ,this.menuItem1label2.y+ this.menuItem1label2.height+75*this.ZOOM,"images" );
        this.menuItem2.frameName='icon/play.png';
        this.menuItem2.scale.setTo(0.8*this.ZOOM);
        this.menuItem2.anchor.setTo(0.5);
        this.menuItem2.inputEnabled =true;
        this.menuItem2.events.onInputDown.add(function(){
        // this.state.start('Game',true,false,{ "save": false, "type":2}); 
        }, this);	

        //this.animPlay(this.menuItem2);
        this.menuGroup.add(this.menuItem2);

        this.menuItem2label = this.game.add.text(this.menuItem2.x+50*this.ZOOM,this.menuItem2.y-30*this.ZOOM, 'Хардкор режим (скоро)', styleText);
        this.menuItem2label2 = this.game.add.text(this.menuItem2.x+50*this.ZOOM,this.menuItem2label.y+this.menuItem2label.height, 'Нет подсказок, Нет сохранений',  {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem2.width+this.menuItem2.x)});


        this.menuItem3 = new Phaser.Sprite(this.game, this.menuItem1.x ,this.menuItem2label2.y+ this.menuItem2label2.height+75*this.ZOOM,"images" );
        this.menuItem3.frameName='icon/play.png';
        this.menuItem3.scale.setTo(0.8*this.ZOOM);
        this.menuItem3.anchor.setTo(0.5);
        this.menuItem3.inputEnabled =true;
        this.menuItem3.events.onInputDown.add(function(){
            //this.state.start('HomeGame2'); 
        }, this);	

        //this.animPlay(this.menuItem3);
        this.menuGroup.add(this.menuItem3);

        this.menuItem3label = this.game.add.text(this.menuItem3.x+50*this.ZOOM,this.menuItem3.y-30*this.ZOOM, 'Кампания (скоро)', styleText);
        this.menuItem3label2 = this.game.add.text(this.menuItem3.x+50*this.ZOOM,this.menuItem3label.y+this.menuItem3label.height, 'Новые правила, новое поведение, много уровней',  {font: 18*this.ZOOM+'px Arial', fill: '#aaa', wordWrap: true, wordWrapWidth: this.game.width-(this.menuItem3.width+this.menuItem3.x)});


        this.menuItem2.alpha=0.4;
        this.menuItem3.alpha=0.4;
    }
    /*
    this.itemSound = new Phaser.Sprite(this.game, (this.game.width)*0.70,this.menuItem.y+100*this.ZOOM,"images" );
    this.itemSound.frameName='icon/gear.png';
    this.itemSound.scale.setTo(0.75*this.ZOOM);
    this.itemSound.anchor.setTo(0.5);

    this.itemSound.inputEnabled =true;
    this.itemSound.events.onInputDown.add(function(){
        this.state.start('Setting');
    }, this);	
    this.menuGroup.add(this.itemSound );

    this.itemHelp = new Phaser.Sprite(this.game, (this.game.width)*0.30,this.menuItem.y+100*this.ZOOM,"images" );
    this.itemHelp.frameName='icon/info.png';
    this.itemHelp.scale.setTo(0.75*this.ZOOM);
    this.itemHelp.anchor.setTo(0.5);
    this.itemHelp.inputEnabled =true;
    this.itemHelp.events.onInputDown.add(function(){
        this.state.start('Help');
    }, this);	
    this.menuGroup.add(this.itemHelp );

    */
    
    /*
    this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+150, 'Как играть?',  {font: '24px Tahoma', fill:'#fff'});
    this.menuItem.anchor.setTo(0.5);   
    this.menuItem.inputEnabled =true;
    this.menuItem.events.onInputDown.add(function(){         
        this.state.start('Game',true,false,{ save: true}); 
    }, this);


    this.menuGroup.add(this.menuItem );*/

    //this.state.start('Game');   

},
update: function(){
    
},
animPlay:function(obj,ran){
        var bounce=this.game.add.tween(obj);
        var ran=ran*-1||10;
    bounce.to({ width:obj.width+ran,height: obj.width+ran,  }, 1000 + Math.random() * 3000, Phaser.Easing.Bounce.In);
    bounce.onComplete.add(function(){

        //	obj.width=temp; obj.height=temp;
            this.animPlay(obj, ran);
        }, this);
    bounce.start();
}
};
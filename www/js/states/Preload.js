var HexGame = HexGame || {};

//loading the game assets
HexGame.PreloadState = {
  preload: function() {
    //show loading screen
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(100, 1);
    this.load.setPreloadSprite(this.preloadBar);

    this.load.image('bubble', 'assets/images/bubble.png');
    this.load.image('help1', 'assets/images/help/1.jpg');
    this.load.image('help2', 'assets/images/help/2.jpg');

    this.load.image('menu1', 'assets/images/menu1.png');
    this.load.image('menu2', 'assets/images/menu2.png');
    this.load.image('menu3', 'assets/images/menu3.png');

    this.load.image('grass2', 'assets/images/hexPurpleFill.png');
    var back= 'assets/images/back800.jpg';
    if (this.game.height>=800) back= 'assets/images/back1200.jpg';
    else   if (this.game.height>1200) back= 'assets/images/back1500.jpg';

    //console.log(back);
    this.load.image('background2',back);

    //data files
    this.load.text('map', 'assets/data/map.json');
    this.load.text('playerUnits', 'assets/data/playerUnits.json');
    this.load.text('enemyUnits', 'assets/data/enemyUnits.json');

		this.load.atlasJSONHash('images', 'assets/images.png', 'assets/images.json');	 

    //sound
    this.load.audio('destroy', ['assets/audio/destroy.ogg', 'assets/audio/destroy.mp3']);
    this.load.audio('win', ['assets/audio/win.ogg', 'assets/audio/win.mp3']);
    this.load.audio('boom', ['assets/audio/boom.ogg', 'assets/audio/boom.mp3']);

  },
  create: function() {
    this.state.start('Game',true,false,{ save: false, "type":3});
  }
};
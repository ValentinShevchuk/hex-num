var HexGame = HexGame || {};
HexGame.Menu = {
    create: function() {

        this.ICON_ZOOM=0.6;
        this.game.world.setBounds(0, 0, this.game.width, this.game.height);
        HexGame.BootState.STATE_NOW='MENU';
        this.ZOOM=HexGame.BootState.ZOOM;
        console.log(this.ZOOM);
        //this.scale.setGameSize(window.innerWidth*1, window.innerHeight*1);
        this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
        this.background.fixedToCamera = true; 
        this.GUI = new mini.GUI(this);  

        this.menuGroup = this.add.group();

        var style= {font: 28*this.ZOOM+'px Arial', fill: '#fff'},
        style2= {font: 18*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-40*this.ZOOM, align: "center"};

        this.TRAINING=3;
        var nameStorage = 'TRAINING';
        if (localStorage.getItem(nameStorage) != null && localStorage.getItem(nameStorage) != undefined) {
            this.TRAINING = localStorage.getItem(nameStorage);
            console.log('загрузка TRAINING');
        }
        //--- название и версия

        text = this.game.add.text(this.game.width*0.5, this.game.world.centerY*0.83, "HexNum",{ 'align':'center', 'font':'Arial', 'fontSize':70*this.ZOOM, 'fontWeight':'bold', 'fill':'#fff'});
        text.anchor.set(0.5);

        //  Here we create our fake reflection :)
        //  It's just another Text object, with an alpha gradient and flipped vertically

        textReflect = this.game.add.text(this.game.width*0.5, this.game.world.centerY*0.83 + 50*this.ZOOM, "HexNum",{'align':'center', 'font':'Arial', 'fontSize':70*this.ZOOM, 'fontWeight':'bold', 'fill':'#fff'});

        //  Centers the text
        textReflect.anchor.set(0.5);
        textReflect.scale.y = -1;

        var grd = textReflect.context.createLinearGradient(0, 0, 0, text.canvas.height);

        //  Add in 2 color stops
        grd.addColorStop(0, 'rgba(255,255,255,0)');
        grd.addColorStop(1, 'rgba(255,255,255,0.08)');

        //  And apply to the Text
        textReflect.fill = grd;

        this.label = this.state.game.add.text(0, 0, 'v.0.4.8', {font: '12px Arial', fill: '#fff'});    
        this.label.anchor.setTo(0.5); 
        this.label.x=text.right-this.label.width/1.7;  
        this.label.y=text.top+(this.label.height/1.5)*this.ZOOM;  



        //---- menu1
        this.menuItem  = this.GUI.iconCreate({'x':this.game.width*0.25, 'y':this.game.height*0.15, 'asset':'menu1', 'scale':0.7*this.ZOOM});
        this.menuItem.events.onInputDown.add(function(){
            this.state.start('Home'); 
        }, this);	
        this.menuItemLabel=this.game.add.text(this.menuItem.x,this.menuItem.bottom+35*this.ZOOM, 'Классическая игра' , {font: 22*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.menuItem.width, align: "center"});
        this.menuItemLabel.anchor.setTo(0.5);

        //----- menu2
        this.menuItem2  = this.GUI.iconCreate({'x':this.game.width*0.75, 'y':this.game.height*0.15, 'asset':'menu2', 'scale':0.7*this.ZOOM});
        this.menuItem2.events.onInputDown.add(function()
        {
            if (this.TRAINING<2) this.state.start('Home2'); 
            else  this.GUI.popupMessage('Пройдите обучение в классическом режиме',this,{  style:{ fill: '#444', font:16*this.ZOOM+'px Arial', align: 'center'}});
        }, this);	
        this.menuItemLabel2=this.game.add.text(this.menuItem2.x,this.menuItem2.bottom+35*this.ZOOM, 'Хардкор игра' , {font: 22*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.menuItem2.width, align: "center"});
        this.menuItemLabel2.anchor.setTo(0.5);


        //---- кампания
        this.menuItem3  = this.GUI.iconCreate({'x':this.game.width*0.25, 'y':this.game.height*0.65, 'asset':'menu3', 'scale':0.7*this.ZOOM});
        this.menuItem3.events.onInputDown.add(function()
        {
            //this.state.start('SelectGame'); 
        }, this);	

        this.menuItemLabel3=this.game.add.text(this.menuItem3.x,this.menuItem3.bottom+35*this.ZOOM, 'Кампания (скоро)' , {font: 22*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.menuItem3.width, align: "center"});
        this.menuItemLabel3.anchor.setTo(0.5);

        this.menuItem3.alpha=0.5;
        this.menuItemLabel3.alpha=0.5;

        //this.animPlay(this.menuItem);
        this.menuGroup.add(this.menuItem );
        /*
        // this.menuItem.y+100*this.ZOOM
        this.itemGear = this.GUI.iconCreate({'x':(this.game.width)*0.85, 'y':this.game.height*(0.50*this.ZOOM), 'frameName':'icon/gear.png', 'scale':0.4*this.ZOOM});
        this.itemGear.events.onInputDown.add(function()
        {
            this.state.start('Setting');
        }, this);	
        this.menuGroup.add(this.itemGear );

        this.itemGear =this.GUI.iconCreate({'x':(this.game.width)*0.85, 'y':this.game.height*(0.61*this.ZOOM), 'frameName':'icon/basket.png', 'scale':0.4*this.ZOOM});
        this.itemGear.events.onInputDown.add(function()
        {
            this.state.start('Shop');
        }, this);	
        this.menuGroup.add(this.itemGear );


        this.itemHelp = this.GUI.iconCreate({'x':(this.game.width)*0.85, 'y':this.game.height*(0.72*this.ZOOM), 'frameName':'icon/info.png', 'scale':0.4*this.ZOOM});
        this.itemHelp.events.onInputDown.add(function()
        {
            this.state.start('Help');
        }, this);	
        this.menuGroup.add(this.itemHelp );
        */
        
        // this.menuItem.y+100*this.ZOOM
        this.itemGear = this.GUI.iconCreate({'x':(this.game.width)*0.57, 'y':this.game.height-100*this.ZOOM, 'frameName':'icon/gear.png', 'scale':0.4*this.ZOOM});
        this.itemGear.events.onInputDown.add(function()
        {
            this.state.start('Setting');
        }, this);	
        this.menuGroup.add(this.itemGear );

        this.itemGear =this.GUI.iconCreate({'x':(this.game.width)*0.74, 'y':this.game.height-100*this.ZOOM, 'frameName':'icon/basket.png', 'scale':0.4*this.ZOOM});
        this.itemGear.events.onInputDown.add(function()
        {
            this.state.start('Shop');
        }, this);	
        this.menuGroup.add(this.itemGear );
        this.itemHelp = this.GUI.iconCreate({'x':(this.game.width)*0.91, 'y':this.game.height-100*this.ZOOM, 'frameName':'icon/info.png', 'scale':0.4*this.ZOOM});
        this.itemHelp.events.onInputDown.add(function()
        {
            this.state.start('Help');
        }, this);	
        this.menuGroup.add(this.itemHelp );
        
        /*
        this.menuItem = this.add.text(this.menuItem.x,this.menuItem.y+150, 'Как играть?',  {font: '24px Tahoma', fill:'#fff'});
        this.menuItem.anchor.setTo(0.5);   
        this.menuItem.inputEnabled =true;
        this.menuItem.events.onInputDown.add(function(){         
        this.state.start('Game',true,false,{ save: true}); 
        }, this);


        this.menuGroup.add(this.menuItem );*/

       

    },
    update: function(){

    },
    animPlay:function(obj,ran){
        var bounce=this.game.add.tween(obj);
        var ran=ran*-1||10;
        bounce.to({ width:obj.width+ran,height: obj.width+ran,  }, 1000 + Math.random() * 3000, Phaser.Easing.Bounce.In);
        bounce.onComplete.add(function(){

        //	obj.width=temp; obj.height=temp;
        this.animPlay(obj, ran);
        }, this);
        bounce.start();
    }
};
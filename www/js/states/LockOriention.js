var HexGame = HexGame || {};
HexGame.LockOriention = {
	init: function(param) {
    this.param=param;
  },

	create: function() {

		this.WIDTH=window.innerWidth;
		this.HEIGHT=window.innerHeight;	

		this.scale.setGameSize(window.innerWidth*1, window.innerHeight*1);
		this.background = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'background');
		this.background.autoScroll(1, 1);

		this.label=this.add.text(this.game.width/2,this.game.height/2-(this.game.height/2)*0.2, 'Переверните экран',  {font: '24px Tahoma', fill: '#fff'});
		this.label.anchor.setTo(0.5);
	},
	update: function(){
		this.WIDTH=window.innerWidth;
		this.HEIGHT=window.innerHeight;	
		if (this.WIDTH<this.HEIGHT){
			this.state.start(this.param.state,true,false,{ save: true}); 
		}
	},
};
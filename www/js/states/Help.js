var HexGame = HexGame || {};
HexGame.Help = {
    init: function(){
        HexGame.BootState.STATE_NOW='HELP';
        this.game.time.advancedTiming = true;

        this.addBoardCount=0;
        this.clickHexCounter=this.addBoardCount;

        this.ZOOM=HexGame.BootState.ZOOM;

        // заглушка

        //this.initGUI();
    },
    create: function() {
  
        this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background2');
        this.background.scale.setTo(this.ZOOM);
        this.game.world.sendToBack(this.background);
        this.background.fixedToCamera = true;

        this.ICON_ZOOM=0.5;


/*
        this.helpGroup = this.add.group();
        //--------
        this.iconBack = this.game.add.sprite(this.game.width - 25*this.ZOOM, 25*this.ZOOM,"images" );
        this.iconBack.frameName='icon/list.png';
        this.iconBack.anchor.setTo(0.5);
        this.iconBack.scale.setTo(this.ICON_ZOOM);
        this.iconBack.inputEnabled =true;
        this.iconBack.events.onInputDown.add(function(){
            this.state.start('Menu'); 
        }, this);	
        this.helpGroup.add(this.iconBack);
        //---------
        this.helpGroup.fixedToCamera = true;
        this.game.world.bringToTop(this.helpGroup);
*/
        this.ICON_ZOOM2=0.8;
        var styleText= {font: 28*this.ZOOM+'px Arial', fill: '#fff'},
        styleText2= {font: 18*this.ZOOM+'px Arial', fill: '#fff', wordWrap: true, wordWrapWidth: this.game.width-50, align: "justify"};

        this.labelText = this.game.add.text(25,25*this.ZOOM, 'Правила игры', styleText);
        this.labelText = this.game.add.text(25,this.labelText.y+45*this.ZOOM, 'Ищите парные числа ', styleText2);
///


        this.block = this.game.add.sprite( this.labelText.x+10*this.ZOOM, this.labelText.y+40*this.ZOOM,"images" );
        this.block.frameName='block/t1.png';
        //this.block.tint=0x66cc99;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '1',  {font: 26*this.ZOOM+'px Arial', fill: "#fff", });	
        num.anchor.setTo(0.5);


        this.block = this.game.add.sprite( this.labelText.x+70*this.ZOOM, this.labelText.y+40*this.ZOOM,"images" );
        this.block.frameName='block/t1.png';
        //this.block.tint=0x66cc99;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '1',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 


//

        this.block = this.game.add.sprite( this.game.width-125, this.labelText.y+40*this.ZOOM,"images" );
        this.block.frameName='block/t7.png';
        //this.block.tint=0xe99785;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '7',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 



        this.block = this.game.add.sprite( this.block.left-90, this.labelText.y+40*this.ZOOM,"images" );
        this.block.frameName='block/t7.png';
        //this.block.tint=0xe99785;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '7',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 



        this.labelText = this.game.add.text(25,this.labelText.y+125*this.ZOOM, 'Либо числа, сумма которых равна 10', styleText2);
/////
        this.block = this.game.add.sprite( this.labelText.x+10*this.ZOOM, this.labelText.y+60*this.ZOOM,"images" );
        this.block.frameName='block/t8.png';
        //this.block.tint=0xd8dabb;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '8',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 


        this.block = this.game.add.sprite( this.labelText.x+70*this.ZOOM, this.labelText.y+60*this.ZOOM,"images" );
        this.block.frameName='block/t2.png';
        //this.block.tint=0xcc6666;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '2',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 



        this.block = this.game.add.sprite( this.game.width-125, this.labelText.y+60*this.ZOOM,"images" );
        this.block.frameName='block/t4.png';
        //this.block.tint=0x4da1ae;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '4',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 


        this.block = this.game.add.sprite( this.block.left-90, this.labelText.y+60*this.ZOOM,"images" );
        this.block.frameName='block/t6.png';
        //this.block.tint=0x36717f;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '6',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 

        //=========================

        this.labelText = this.game.add.text(25,this.block.y+this.block.height+20*this.ZOOM, 'Или сумма кратна 10 в режиме "хардкор"', styleText2);

        //=========================
        this.block = this.game.add.sprite( this.labelText.x+10*this.ZOOM, this.labelText.bottom+10*this.ZOOM,"images" );
        this.block.frameName='block/t2.png';
        //this.block.tint=0xd8dabb;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '12',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 


        this.block = this.game.add.sprite( this.labelText.x+70*this.ZOOM, this.labelText.bottom+10*this.ZOOM,"images" );
        this.block.frameName='block/t8.png';
        //this.block.tint=0xcc6666;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '28',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 



        this.block = this.game.add.sprite(this.game.width-125, this.labelText.bottom+10*this.ZOOM,"images" );
        this.block.frameName='block/t9.png';
        //this.block.tint=0x4da1ae;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '9',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 


        this.block = this.game.add.sprite( this.block.left-90, this.labelText.bottom+10*this.ZOOM,"images" );
        this.block.frameName='block/t1.png';
        //this.block.tint=0x36717f;
        this.block.scale.setTo(this.ICON_ZOOM2);

        var num = this.state.game.add.text(this.block.x+(this.block.width/2), this.block.y+(this.block.height/2), '11',  {font: 26*this.ZOOM+'px Arial', fill: "#fff"});	
        num.anchor.setTo(0.5); 



        this.labelText = this.game.add.text(25,this.block.y+this.block.height+40*this.ZOOM, 'Числа можно выбрать если они стоят в определённом направлении друг от друга: слева, справа и по диагонали. Возможные варианты автоматически подсвечиваются.', styleText2);

        this.helpBlock = this.game.add.sprite( 50, this.labelText.y+this.labelText.height+20*this.ZOOM,"help1" );
        this.helpBlock = this.game.add.sprite( 50, this.helpBlock.y+this.helpBlock.height+20*this.ZOOM,"help2" );	


        this.labelText = this.game.add.text(25,this.helpBlock.y+this.helpBlock.height+40*this.ZOOM, 'Если вариантов найти не удается, воспользуйтесь платными подсказками или добавьте новые числа', styleText2);

        this.helpBlock = this.game.add.sprite( 50, this.labelText.y+this.labelText.height+20*this.ZOOM,"images" );
        this.helpBlock.frameName='icon/light-bulb.png';
        //this.helpBlock.anchor.setTo(0.5);
        this.helpBlock.scale.setTo(this.ICON_ZOOM);

        this.labelText = this.game.add.text(this.helpBlock.x+this.helpBlock.width+10,this.helpBlock.y, 'Найти и подсветить пару', styleText2);


        this.helpBlock = this.game.add.sprite( 50, this.labelText.y+this.labelText.height+50*this.ZOOM,"images" );
        this.helpBlock.frameName='icon/undo.png';
        //this.helpBlock.anchor.setTo(0.5);
        this.helpBlock.scale.setTo(this.ICON_ZOOM);

        this.labelText = this.game.add.text(this.helpBlock.x+this.helpBlock.width+10,this.helpBlock.y, 'Отменить действие', styleText2);

        this.helpBlock = this.game.add.sprite( 50, this.labelText.y+this.labelText.height+50*this.ZOOM,"images" );
        this.helpBlock.frameName='icon/add.png';
        //this.helpBlock.anchor.setTo(0.5);
        this.helpBlock.scale.setTo(this.ICON_ZOOM);

        this.labelText = this.game.add.text(this.helpBlock.x+this.helpBlock.width+10,	this.helpBlock.y, 'Кнопка добавляет числа на поле. Добавляются только числа, имеющиеся на поле. Добавление новых чисел недоступно пока есть доступные комбинации и кол-во блоков превышает максимально возможное количество чисел', styleText2);

        this.labelText.wordWrapWidth-=this.helpBlock.x+this.helpBlock.width;

      this.topGroup = this.add.group();
    //--------
    this.iconBack = this.game.add.sprite(this.game.width - 25*this.ZOOM, 25*this.ZOOM,"images" );
    this.iconBack.frameName='icon/list.png';
    this.iconBack.anchor.setTo(0.5);
    this.iconBack.scale.setTo(this.ICON_ZOOM*0.7);
    this.iconBack.inputEnabled =true;
    this.iconBack.events.onInputDown.add(function(){
    this.state.start('Menu'); 
    }, this);	
    this.iconBack.fixedToCamera = true;
    this.topGroup.add(this.iconBack);
    //--------- 

        this.kineticScrolling = this.game.plugins.add(Phaser.Plugin.KineticScrolling);
        this.kineticScrolling.configure({
            verticalScroll: true,
            horizontalScroll: false
        });
        this.kineticScrolling.start();
        
        this.state.game.world.setBounds(0, 0, this.state.game.width, this.labelText.y+this.labelText.height+250);
            //vthis.scale.setGameSize(window.innerWidth*1, this.helpBlock.y+this.helpBlock.height);
            //console.log(this.helpBlock.y+this.helpBlock.height);
    },


    
    initGUI: function () {
        this.GUI = new mini.GUI(this);  
        var styleText= {font: '28px Arial', fill: '#fff'},
        styleText2= {font: '18px Arial', fill: '#fff'};
           
    this.panel = this.add.group();

        this.icon = this.add.sprite(30, this.game.height-30, 'images');
        this.icon.frameName='icon/add.png';
        this.icon.anchor.setTo(0.5);
        this.icon.inputEnabled = true;


        this.icon.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else {
                this.addBoardCount++;
                this.addBoard2();
            }
        }, this);
        this.panel.add(this.icon);

        //icon удаления
        this.iconDel = this.add.sprite( this.game.width-30, this.game.height-30, 'images');
        this.iconDel.frameName='icon/del.png';
        this.iconDel.anchor.setTo(0.5);
        this.iconDel.inputEnabled = true;
        this.iconDel.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else { 
                this.addBoardCount++;
                if (this.userStat.coin>0) {
                    this.GUI.vanishMessage('Выберите цифру для уничтожения\nКликните по ней два раза',this.iconDel,{  style:{ fill: '#fff', font:'14px Tahoma', align: 'right'}});

                    this.iconDel.alpha=this.iconDel.alpha==1?0.5:1; 
                } else {
                    this.GUI.vanishMessage('Нет средств, закройте хотя бы одну пару',this.iconDel,{  style:{ fill: '#fff', font:'14px Tahoma', align: 'right'}});
                }
            }
        }, this);
        this.panel.add(this.iconDel);


        //icon локатор
        this.iconLoc = this.add.sprite( this.game.width-80, this.game.height-30, 'images');
        this.iconLoc.frameName='icon/locator.png';
        this.iconLoc.anchor.setTo(0.5);
        this.iconLoc.inputEnabled = true;
        this.iconLoc.events.onInputDown.add(function(){
            if (this.addBoardCount==1){this.addBoardCount=0;}
            else { 
                this.addBoardCount++;
                if (this.userStat.coin>0) {
                    this.GUI.vanishMessage('Выберите цифру. Стоимость подсказки 1$',this.iconLoc,{  style:{ fill: '#fff', font:'14px Tahoma', align: 'right'}});

                    this.iconLoc.alpha=this.iconLoc.alpha==1?0.5:1; 
                } else {
                    this.GUI.vanishMessage('Нет средств, закройте хотя бы одну пару',this.iconLoc,{  style:{ fill: '#fff', font:'14px Tahoma', align: 'right'}});
                }
            }
        }, this);
        this.panel.add(this.iconLoc);


var help=[
    "Выберите пару:\n\n1. С одинаковые цифрами\n2. Либо блоки сумма которых равна 10"
];
var helpIndex=0;

        // метка с временем игры
        this.labelText = this.game.add.text(25,(this.game.height/2), help[helpIndex], styleText2);
    //this.label.fixedToCamera = true;  
        //this.labelText.anchor.setTo(0.5);
    this.panel.add(this.labelText);



    //	this.animPlay(this.labelText);

        this.panel.fixedToCamera = true;
        this.game.world.bringToTop(this.panel);






    },
helpTextAnim:function(){
        var bounce=this.game.add.tween(obj);
        var ran=ran*-1||10;
    bounce.to({ width:obj.width+ran,height: obj.width+ran,  }, 1000 + Math.random() * 3000, Phaser.Easing.Bounce.In);
    bounce.onComplete.add(function(){

        //	obj.width=temp; obj.height=temp;
            this.animPlay(obj, ran);
        }, this);
    bounce.start();
}

};